package de.dhbw.foodcoops.client.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import de.dhbw.foodcoops.js.ILogAction;
import de.dhbw.foodcoops.js.PlatformServices;
import java.io.PrintStream;
import java.util.Locale;

class AndroidServices extends PlatformServices {

    private final SharedPreferences prefs;
    private final Context context;

    AndroidServices(SharedPreferences prefs, Context context) {
        this.prefs = prefs;
        this.context = context;
    }

    @Override
    public void alert(String msg) {
        new AlertDialog.Builder(context)
                .setMessage(msg)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

//    @Override
//    public void confirmByUser(String msg, Runnable callback) {
//        new AlertDialog.Builder(context)
//                .setMessage(msg)
//                .setPositiveButton("Bestätigen", (di, i) -> callback.run())
//                .setNegativeButton("Abbrechen", null)
//                .show();
//    }

    @Override
    public String getPreferences(String key) {
        return prefs.getString(key, null);
    }

    @Override
    public void setPreferences(String key, String value) {
        prefs.edit().putString(key, value).apply();
    }

    @Override
    public void decorateSystemLogger(ILogAction standardLogCallback, ILogAction infoLogCallback, ILogAction warnLogCallback, ILogAction errorLogCallback) {
        super.decorateSystemLogger(standardLogCallback, infoLogCallback, warnLogCallback, errorLogCallback);

        final PrintStream standardStream = System.out;
        final PrintStream errorStream = System.err;

        System.setOut(new LogDecorator(standardStream, standardLogCallback));
        System.setErr(new LogDecorator(errorStream, warnLogCallback));
    }

    private static class LogDecorator extends PrintStream {

        private final ILogAction action;
        private final PrintStream decoratedStream;

        public LogDecorator(PrintStream decoratedStream, ILogAction action) {
            super(decoratedStream, true);
            this.action = action;
            this.decoratedStream = decoratedStream;
        }

        @Override
        public void println() {
            decoratedStream.println();
            action.log("");
        }

        @Override
        public void println(String x) {
            decoratedStream.println(x);
            action.log(x);
        }

        @Override
        public void println(Object x) {
            decoratedStream.println(x);
            action.log(String.valueOf(x));
        }

        @Override
        public void println(int x) {
            decoratedStream.println(x);
            action.log(String.valueOf(x));
        }

        @Override
        public void println(double x) {
            decoratedStream.println(x);
            action.log(String.valueOf(x));
        }

        @Override
        public void println(char x) {
            decoratedStream.println(x);
            action.log(String.valueOf(x));
        }

        @Override
        public void println(boolean x) {
            decoratedStream.println(x);
            action.log(String.valueOf(x));
        }

        @Override
        public void println(char[] x) {
            decoratedStream.println(x);
            action.log(String.valueOf(x));
        }

        @Override
        public void println(float x) {
            decoratedStream.println(x);
            action.log(String.valueOf(x));
        }

        @Override
        public void println(long x) {
            decoratedStream.println(x);
            action.log(String.valueOf(x));
        }

        @Override
        public void print(Object obj) {
            this.println(obj);
        }

        @Override
        public void print(int obj) {
            this.println(obj);
        }

        @Override
        public void print(double obj) {
            this.println(obj);
        }

        @Override
        public void print(char obj) {
            this.println(obj);
        }

        @Override
        public void print(char[] obj) {
            this.println(obj);
        }

        @Override
        public void print(float obj) {
            this.println(obj);
        }

        @Override
        public void print(long obj) {
            this.println(obj);
        }

        @Override
        public void print(String obj) {
            this.println(obj);
        }

        @Override
        public void print(boolean obj) {
            this.println(obj);
        }

        @Override
        public PrintStream printf(String format, Object... args) {
            String message = String.format(format, args);
            this.print(message);
            return this;
        }

        @Override
        public PrintStream printf(Locale l, String format, Object... args) {
            String message = String.format(l, format, args);
            this.print(message);
            return this;
        }
    }
}
