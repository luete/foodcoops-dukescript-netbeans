# Setup (Development environment)
For developing the Netbeans Project you are required to have the following tools installed and properly configured.

* (Open-)JDK 8 <span style="color:red">*</span>
    * **We use:** OpenJDK 8
    * **Why Java 8?** The AndroidSDK requires Java 8, so it is used across all projects
    * **Why OpenJDK?** You need to register to Oracle to be able to download the *Oracle JDK*
* Maven <span style="color:red">*</span>
    * **We use:** Maven 3.6.3
    * **Required for:** Building and managaging projects, dependency management ...
* Apache NetBeans
    * **We use:** Apache Netbeans 11.2
    * **Why specifically Netbeans?:** A Java Preprocessor that generates the DukeScript Model classes, does not properly work with eclipse.
* Android Studio
    * **We use:** Android Studio 3.5.3
    * **Contains:**
        * Android SDK <span style="color:red">*</span>
            * **Required for:** Building the android project 
        * AVD Manager  
        *(contained in Android SDK)*
            * **Required for:** Managing and starting virtual android devices.  
            Only required for testing on emulated devices
* Web-Server <span style="color:red">**</span>
    * **We use:** Tomcat 9
    * **Required for:** Running the Server with the REST API (deployed as *war* file)  
    It is only required for the actual Server or if you want to host it in a standalone variant.

<span style="color:red">*</span> Might require configuration of _environment variables_  
<span style="color:red">**</span> Not required for development

## Configure environment variables
Environment variables must be configured for the tools in the following listing. This is usually only required if you have multiple versions of the same tool installed or when something went wrong during the installation.

### OpenJDK 8
**Variables:** JAVA_HOME  
Path to the root directory of the JDK installation
### Maven
**Variables:** MAVEN_HOME, PATH  
- Set *MAVEN_HOME* to the path of the root directory of the maven installation 
- Add an entry to *PATH* that points to *%MAVEN_HOME%\bin*

### AndroidSDK
**Variables:** ANDROID_HOME  
*Needs to be configured for the maven-android build plugin* 
- Set *ANDROID_HOME* to the AndroidSDK installation directory.  
For example: *"D:\Android\sdk"*
- Can alternatively be passed as maven property argument.  
For example: *-Dandroid.sdk.path="D:\Android\sdk"*


## Setup Android Environment
The following section describes what is required to be ready for building android projects and to run them on emulated devices.

**Build prerequisites:**  
You'll need to install the SDK with **API-Level 28** (The version is defined in the Android Projects pom, in case this gets outdated).  
Via AndroidStudio: "MenuBar > Tools > SDK Manager > Select Version > Ok"

Other than that, you'll only need to configure maven/environment variables with your AndroidSDK installation directory.

**Emulation prerequisites:**  
To run the project on emulated devices, you have to add one with the help of the *AVD Manager* Tool.
Note that the virtual device requires an *API Level of minimum 19*, which is the minimum- and target-API Level of the project (specified in the AndroidManifest.xml).  
Via AndroidStudio: "MenuBar > Tools > AVD Manager > Create Virtual Device > Configure to your needs"

Since the virtual device won't be started by maven, you'll have to start it once by yourself before you can run the application on it.

# Run and Build from CommandLine
The scripts and commands in this chapter require you to either use a Linux shell.  That means you either use a linux system, the Linux Subsystem for Windows or the git bash (it is also a reduced bash shell).

Source the build-aliases.sh file to get access to aliases for the bash-shell. They help you with building and running the projects with specific configurations and as efficiently as possible.

```bash
cd PROJECT_FOLDER #Your project folder
source _tools/build-aliases.sh
```

Every created alias is prefixed with _"fc-"_, to enable easy auto-completion.

## Build commands
* fc-clean-and-build
    * *Cleans cache and builds all projects*
* fc-run-server
    * *Cleans cache and builds server-project with dependencies only. Then starts server-project in an embedded environment*
* fc-run-web-{app}
    * *Cleans cache and builds web-project with dependencies only. Then starts web-project*
* fc-run-android-{app}
    * *Cleans cache and build android-project with dependencies only. Then starts android-project*
* fc-remove-maven-artifacts
    * *Removes all maven artifacts of the food-coops group from the local maven repository*

### Build different apps
Some build commands have the suffix -{app}. This means you have to specify which app you want to build. You can build the following apps: "admin", "user"  
Example: fc-run-web-admin

# Maven Projects
The Maven projects are organized in such a way, that the root directory contains a pom file which is inherited by all other projects. This pom also aggregates all child-projects as modules, so you can build them from one place (the root directory).

There are basically 3 types of (child-)projects. The _Server_ project is a special case and will not be listed below.
* Platform specific projects
    * **Task:** Implement platform specific services and inject them to (as well as configure) the apps general client code
    * **Example:** client-android, client-web
    * **Name-Prefix:** client-
    * **Referenced by:** No other projects
* General App-Client Code
    * **Task:** Provide the actual app content and logic. Configurable by accepting _Service Provider Interfaces_, which are injected by the platform specific projects.
    * **Example:** client
    * **Name-Prefix:** <span style="color:red">To be defined</span>
    * **Referenced by:** Platform specific projects
* Library projects
    * **Task:** Provide _Service Provider Interfaces_ for platform specific code, which are implemented and injected by the platform specific projects.
    * **Example:** database, model
    * **Name-Prefix:** <span style="color:red">To be defined</span>
    * **Referenced by:** All project types

## Apps
As explained before, we have one Java-Project for each specific platform. Different apps define a set of properties which will be replaced in specific files (e.g. "index.html").

The logic & files of the different apps are all located in the "General App-Client Code".
All of the platform projects (general-client-code, client-web, client-android) have 1 main File for each app. Which app will be built, depends one the properties which specified in the parent pom.

For easier use, all properties are set in a single profile for the specific app.

## Add new Project (Module)
To add a new Project you'll have to follow the steps below.  
1. Create Maven Project in the root directory
2. Add it as a Module in the top-level .pom file.  
Well, this is actually optional, but we do this to make sure every project is built on a single mvn compile in the root project/directory.
```xml
<modules>
    <!-- ... -->
    <module>DIRECTORY_NAME_OF_YOUR_MODULE</module>
    <!-- ... -->
</modules>
```
3. Inherit from the parent pom (Add this in the new projects _pom.xml_)
```xml
<parent>
    <groupId>de.dhbw.foodcoops</groupId>
    <artifactId>pom</artifactId>
    <version>1.0-SNAPSHOT</version>
</parent>
```
4. (Optional) Add it as a Maven dependency where required

### Add a Library Project
Since DukeScript uses OSGI in the background, all library projects (projects that're intended to be referenced) have to be bundled for OSGI and export public packages.
These configurations have to be done in the projects .pom file.

You can set the packaging as followed:
```xml
<project>
    <!-- ... -->
    <packaging>bundle</packaging>
    <!-- ... -->
</project>
```

Also you have to add the maven-bundle plugin and export all relevant public packages.
For the Bundle-Symbolic name, we suggest to use the groupID + artifactID as used in the maven pom.  
**Example:**
```xml
<plugin>
    <groupId>org.apache.felix</groupId>
    <artifactId>maven-bundle-plugin</artifactId>
    <version>2.4.0</version>
    <extensions>true</extensions>
    <configuration>
        <instructions>
            <Export-Package>de.dhbw.foodcoops.*</Export-Package>
            <Bundle-SymbolicName>de.dhbw.foodcoops.database</Bundle-SymbolicName>
        </instructions>
    </configuration>
</plugin>
```

# Developers Guide
This chapter makes you familiar with the projects structure and the way it is developed. It's goal is to give you entry points and teach you how to do certain tasks, like adding new pages and views.
We will not explain to you how these technologies work, but we'll link to their documentation.  
The following links describe the basics for the user interface:
* [DukeScript Getting started](https://www.dukescript.com/getting_started.html)
    * **What it is:** A Multi-Platform technology for developing Java Apps
    * **What you can learn:** DukeScript-ViewModel basics, Simple DataBinding, Wrapping JavaScript functions & libraries
* [KnockoutJS Documentation](https://knockoutjs.com/documentation/introduction.html)
    * **What it is:** A JavaScript MVVM framework
    * **What you can learn:** How to use DataBinding, How to write HTML templates
* [OnsenUI Documentation](https://onsen.io/v2/guide/#getting-started)
    * **What it is:** A Web-Component library focused on mobile apps
    * **What you can learn:** How to use components


## User Interface
The Project uses the Model View ViewModel (MVVM) Pattern to separate UI and model.
All Data-Model classes as they're used in the apps and on the server have their home in the _model_ project.  
All code that controls the view and the apps lie in the _client_ project.
The view itself is written in html and css. The data that is displayed and written is bound to _ViewModel_ properties. They're automatically updated when the ViewModel changes or it is triggered by user input.

### ViewModels
All ViewModels are defined in the _client_ project as DukeScript Models.  
They're annotated in a definition class with `@Model` and define properties & methods that can be bound to the view and used to generate classes. See the DukeScript Documentation for further explanation.  
**Our naming convention:** Use a _Def_ suffix for defintion classes (after the name of the generated class-name)

DukeScript doesn't allow custom constructors for the ViewModels, which can have a quite complex initialization and otherwise only be initialized with getters and methods. To do this initialization and have a single point & way to create instances of a ViewModel we use static factory methods.  
**Our naming convention:** Just called _create_, for more semantic uses prefix the method name with _create_

Initialization of services that can't be added as `@Property` will require the declaration of a field and a `@ModelOperation` to set the value of the field. 
You should never use the _init_ method outside of the _create_ factory methods. Sadly they must be public and we can't prevent anyone from doing so.  
**Our naming convention:** Use the field name with the prefix _init_. Example: FieldName = services → MethodName = initServices

```java
@Model(className = "MemberManipulationViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "pageTitle", type = String.class),
            @Property(name = "itemToManipulate", type = Member.class)
        })
public class MemberManipulationViewModelDef
{
    private IPlatformServices services;

    public static MemberManipulationViewModelDef create(IPlatformServices services/*, more parameters for initialization */)
    {
        /* Initialization logic, this method acts as constructor replacement */
        MemberManipulationViewModel vm = new MemberManipulationViewModel();
        vm.initServices(services);
        return vm;
    }

    /* .. */

    @Function
    public void save(MemberManipulationViewModel model) { /* ... */ }

    // IPlatformServices can't be added as a property (since its not @Model annotated)
    // and therefore needs to be initialized in this hacky way
    @ModelOperation
    public void initServices(MemberManipulationViewModel model, IPlatformServices services)
    {
        this.services = services;
    }
}
```

Since many parts of the application consist of similar types of pages and tasks for different model types theres a common structure in many ViewModels.
There is a RootViewModel for each Application (user and admin app), which is bound to the body of the index file. From there a ViewModel-Hierarchie is built, which builds a tree of different contexts. Each ViewModel can serve as the context of different Views in the application.

## Backend
### Respositories
To retrieve, store and update domain models in the backend we use the _repository pattern_.
There is a generic interface named `IRepository<TManaged>`, where `TManaged` is the type of the managed object.

Each domain model has its own repository with the following operations:
* `void findById(int id, IResultHandler<TManaged> resultHandler);`
* `void remove(TManaged obj, IResultHandler<Void> resultHandler);`
* `void add(TManaged obj, IResultHandler<Void> resultHandler);`
* `void getAll(IResultHandler<List<TManaged>> resultHandler);`
* `void update(TManaged obj, IResultHandler<Void> resultHandler);`

There are different implementations for all of the repositories:
* `...RestRepository`
  * Used on the client for communication with the server
* `...AndroidRepository`
  * Used on the client as a fallback when the server is down
* `...JdbcRepository`
  * Used on the server for the local database

#### Result Handling
Every function of the repository accepts an `IResultHandler<TResult>` (where `TResult` is the type passed on successful execution) as a callback to enable handling of successful or failed executions of the operation in asynchronous ways. Methods like add, that don't return a value and can only succeed or fail accept an `IResultHandler<Void>`.

Each implementation of `IResultHandler<TResult>` has to implement the following methods (of which only one will be called, and also only once per operation):
* `void onSuccess(TResult result);`
* `void onError(Exception errorDetails);`

#### Usage example
```java
// Create instance of a repository communicating via a REST api
IRepository<Member> memberRepository = new MemberRestRepository();

// Retrieve all members using a ResultHandler (anonymous class)
memberRepository.getAll(new IResultHandler<List<Member>>()
{
    public void onSuccess(List<Member> result)
    {
        // do something with the result
    }

    public void onError(Exception ex)
    {
        // handle the error
    }
});
```
