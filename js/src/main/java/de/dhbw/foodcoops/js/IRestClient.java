package de.dhbw.foodcoops.js;

public interface IRestClient {

    public void sendAsync(String url, String method, String data, IRestCallback restCallback);

    public void send(String url, String method, String data, IRestCallback restCallback);
}
