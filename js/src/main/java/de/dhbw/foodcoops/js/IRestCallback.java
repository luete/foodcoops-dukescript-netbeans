package de.dhbw.foodcoops.js;

public interface IRestCallback {
    public void onSuccess(int statusCode, String responseText);
    public void onError(String errorText);
}
