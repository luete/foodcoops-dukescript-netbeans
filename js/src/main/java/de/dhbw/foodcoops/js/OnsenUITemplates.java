package de.dhbw.foodcoops.js;

import net.java.html.js.JavaScriptBody;

public class OnsenUITemplates {

    //Stackoverflow question
//    @JavaScriptBody(args = {"name", "pageViewModel"},
//            body
//            = "document.querySelector('ons-navigator')\n"
//            + "       .pushPage(name, { data: pageViewModel });\n"
//            + "console.log('Switched page to: ' + name);\n"
//            + "console.log('PageViewModel: ' + JSON.stringify({ data: pageViewModel }));")
//Stackoverflow solution
//    @JavaScriptBody(args = {"name", "pageViewModel"},
//            body
//            = "document.querySelector('ons-navigator')\n"
//            + "       .pushPage(name, { data: { viewModel: pageViewModel } });\n"
//            + "console.log('Switched page to: ' + name);\n"
//            + "console.log('PageViewModel: ' + JSON.stringify({ data: { viewModel: pageViewModel } }));")
//    
//normal one
    
    /**
     * Adds the specified page to the stack and views it.
     * 
     * @param name The path/name of the page you want to navigate to.
     */
    @JavaScriptBody(args = {"name"},
            body = "document.querySelector('#page-content-navigator')\n"
            + "       .pushPage(name);")
    public static native void switchPage(String name);

    /**
     * Removes the current page from the stack and goes one back.
     */
    @JavaScriptBody(args = {},
            body = "document.querySelector('#page-content-navigator').popPage();")
    public static native void goBackOnePage();

    @JavaScriptBody(args = {"name"},
            body = "document.querySelector('#page-content-navigator')\n"
            + "       .resetToPage(name);")
    public static native void resetToPage(String name);

    @JavaScriptBody(args = {},
            body = "document.addEventListener('init', function(event) {\n"
            + "    var page = event.target;\n"
            + "    /*ko.applyBindings(ko.dataFor(page), page);*/"
            + "    ko.applyBindings(ko.dataFor(document.getElementById('body')), page);\n"
            + "  }); ")
    public static native void registerListener();
    
    @JavaScriptBody(args = {},
            body = "let menu = document.getElementById('sidebar');\n" +
                    "menu.toggle();")
    public static native void toggleSidebar();
    
    @JavaScriptBody(args = {},
            body = "let menu = document.getElementById('sidebar');\n" +
                    "menu.close();")
    public static native void closeSidebar();
}
