function xhrSuccess() {
//    console.log("Success: " + JSON.stringify(arguments)); 
}

function xhrError(errorCallback, args) {
    var message = "An unknown error occured. Probably a network error.\n\
                     Possible issues: Blocked by CORS | Server down | SSL issue";

    if (this.statusText !== null && this.statusText !== undefined && this.statusText !== "")
    {
        message = this.statusText;
    }

//    console.log("Error callback args: " + JSON.stringify(args));
    errorCallback("Status: '" + message + "'");
}

function xhrSend(async, url, successCalback, errorCallback, method, data, username, password, appId) {
    let xhr = new XMLHttpRequest();
    xhr.arguments = Array.prototype.slice.call(arguments, 3);
    xhr.onload = xhrSuccess;
    xhr.onerror = function (args) {
        xhrError(errorCallback, args);
    };

    xhr.onreadystatechange = function ()
    {
        if (xhr.readyState == 4)
        {
//            console.log("Response("  + url +  " | " + method + " | " + data +") ---- Status(" + xhr.status + "): " + xhr.responseText); // Another callback here
            if (xhr.status >= 200 && xhr.status < 300)
            {
                successCalback(xhr.status, xhr.responseText);
            }
            else
            {
                errorCallback(xhr.responseText);
            }
        }
    };

    xhr.open(method, url, async);


    //Use empty strings if no credentials are provided
    username = username ? username : "";
    password = password ? password : "";

    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password))
    xhr.setRequestHeader("FC-App", appId);
    
    xhr.send(data);
}

