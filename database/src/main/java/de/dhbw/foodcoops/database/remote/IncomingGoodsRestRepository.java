package de.dhbw.foodcoops.database.remote;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IRestClient;
import de.dhbw.foodcoops.model.IncomingGoods;
import java.util.List;

public class IncomingGoodsRestRepository extends RestRepositoryBase<IncomingGoods> {

    public IncomingGoodsRestRepository(IRestClient restclient) {
        super("incominggoods", IncomingGoods.class, restclient, (m) -> m.getId());
    }
}
