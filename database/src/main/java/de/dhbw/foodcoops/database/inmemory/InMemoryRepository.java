package de.dhbw.foodcoops.database.inmemory;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.database.resulthandling.Void;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class InMemoryRepository<T> implements IRepository<T> {

    private int itemCounter = 0;
    private final List<T> items = new ArrayList<>();
    private final IIdentityProvider<T> identityProvider;
    private final IIdentitySetter<T> identitySetter;

    public InMemoryRepository(IIdentityProvider<T> identityProvider, IIdentitySetter<T> identitySetter) {
        this.identityProvider = identityProvider;
        this.identitySetter = identitySetter;
    }

    @Override
    public void getAll(IResultHandler<List<T>> resultHandler) {
        resultHandler.onSuccess(Collections.unmodifiableList(items));
    }

    @Override
    public void add(T obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        addWithId(itemCounter++, obj, resultHandler);
    }

    private void addWithId(int id, T obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        identitySetter.setId(id, obj);
        items.add(obj);

        resultHandler.onSuccess(de.dhbw.foodcoops.database.resulthandling.Void.INSTANCE);
    }

    @Override
    public void remove(T obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        int id = identityProvider.idFor(obj);
        T itemToRemove = findById(id);

        if (itemToRemove != null) {
            items.remove(itemToRemove);
            resultHandler.onSuccess(de.dhbw.foodcoops.database.resulthandling.Void.INSTANCE);
        } else {
            resultHandler.onError(new RuntimeException("Could not remove item with id " + id));
        }
    }

    @Override
    public void update(T obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        int id = identityProvider.idFor(obj);
        T value = findById(id);

        if (value != null) {
            remove(value, new IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void>() {
                @Override
                public void onSuccess(Void result) {
                    addWithId(id, obj, resultHandler);
                }

                @Override
                public void onError(Exception ex) {
                    resultHandler.onError(new Exception("Failed to remove items for update. Original Exception: " + ex, ex));
                }
            });
        } else {
            resultHandler.onError(new Exception("Item was not found in database (InMemoryRepository)."));
        }
    }

    @Override
    public void findById(int id, IResultHandler<T> resultHandler) {
        for (T item : items) {
            int itemId = identityProvider.idFor(item);
            if (itemId == id) {
                resultHandler.onSuccess(item);
                return;
            }
        }

        resultHandler.onError(new RuntimeException("Could not find item with id: " + id));
    }

    private T findById(int id) {
        for (T item : items) {
            int itemId = identityProvider.idFor(item);
            if (itemId == id) {
                return item;
            }
        }

        return null;
    }
}
