package de.dhbw.foodcoops.database.resulthandling;

/**
 * ResultHandler implementation that performs no access.
 */
public final class NullResultHandler<T> implements IResultHandler<T> {

    @Override
    public void onSuccess(T result) {
    }

    @Override
    public void onError(Exception ex) {
    }

}
