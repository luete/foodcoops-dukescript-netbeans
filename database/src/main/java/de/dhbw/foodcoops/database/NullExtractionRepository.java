package de.dhbw.foodcoops.database;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.database.resulthandling.Void;
import de.dhbw.foodcoops.model.Extraction;
import java.util.List;

public class NullExtractionRepository implements IExtractionRepository {
    @Override
    public void add(Extraction obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
    }

    @Override
    public void getAll(IResultHandler<List<Extraction>> resultHandler) {
    }

    @Override
    public void remove(Extraction obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
    }

    @Override
    public void update(Extraction obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
    }

    @Override
    public void findById(int id, IResultHandler<Extraction> resultHandler) {
    }

    @Override
    public void pay(int id, IResultHandler<Void> resultHandler) {
    }
}