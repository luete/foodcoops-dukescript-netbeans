package de.dhbw.foodcoops.database;

import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.database.resulthandling.Void;
import java.util.List;

/**
 * Decorates a 'Main' repository. If the operation fails, it will try to use the
 * fallback.
 *
 * @param <T> Type of the managed entity
 */
public class CompositeRepository<T> implements IRepository<T> {

    private final IRepository<T> mainRepository;
    private final IRepository<T> fallbackRepository;

    public CompositeRepository(IRepository<T> mainRepository, IRepository<T> fallbackRepository) {
        this.mainRepository = mainRepository;
        this.fallbackRepository = fallbackRepository;
    }

    @Override
    public void remove(T obj, IResultHandler<Void> resultHandler) {
        mainRepository.remove(obj, new DelegateResultHandler<>(
                (result) -> {
                    resultHandler.onSuccess(result);
                },
                (exception) -> {
                    System.err.println("Failed 'remove', use Fallback: " + exception);
                    fallbackRepository.remove(obj, resultHandler);
                }));
    }

    @Override
    public void add(T obj, IResultHandler<Void> resultHandler) {
        mainRepository.add(obj, new DelegateResultHandler<>(
                (result) -> {
                    resultHandler.onSuccess(result);
                },
                (exception) -> {
                    System.err.println("Failed 'add' on main: " + exception);
                    fallbackRepository.add(obj, resultHandler);
                }));
    }

    @Override
    public void getAll(IResultHandler<List<T>> resultHandler) {
        mainRepository.getAll(new DelegateResultHandler<>(
                (result) -> {
                    resultHandler.onSuccess(result);
                },
                (exception) -> {
                    System.err.println("Failed 'getAll' on main: " + exception);
                    fallbackRepository.getAll(resultHandler);
                }));
    }

    @Override
    public void update(T obj, IResultHandler<Void> resultHandler) {
        mainRepository.update(obj, new DelegateResultHandler<>(
                (result) -> {
                    resultHandler.onSuccess(result);
                },
                (exception) -> {
                    System.err.println("Failed 'update' on main: " + exception);
                    fallbackRepository.update(obj, resultHandler);
                }));
    }

    @Override
    public void findById(int id, IResultHandler<T> resultHandler) {
        mainRepository.findById(id, new DelegateResultHandler<>(
                (result) -> {
                    resultHandler.onSuccess(result);
                },
                (exception) -> {
                    System.err.println("Failed 'findById' on main: " + exception);
                    fallbackRepository.findById(id, resultHandler);
                }));
    }
}
