package de.dhbw.foodcoops.database.remote;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IRestClient;
import de.dhbw.foodcoops.model.WarehouseItem;
import java.util.List;

public class WarehouseRestRepository extends RestRepositoryBase<WarehouseItem> {
    public WarehouseRestRepository(IRestClient restclient) {
        super("warehouseitems", WarehouseItem.class, restclient, (m) -> m.getId());
    }
}
