package de.dhbw.foodcoops.database.resulthandling;

/**
 * Service Provider interface for handling results of asynchronous operations.
 * @param <T> Type of the operations result. If the operation doesn't provide a result use {@link de.dhbw.foodcoops.action.Void}
 */
public interface IResultHandler<T> {
    void onSuccess(T result);
    
    void onError(Exception ex);
}
