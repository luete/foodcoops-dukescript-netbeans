package de.dhbw.foodcoops.database.remote;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IRestClient;
import de.dhbw.foodcoops.model.ShoppingCart;
import java.util.List;

public class ShoppingCartRestRepository extends RestRepositoryBase<ShoppingCart> {

    public ShoppingCartRestRepository(IRestClient restClient) {
        super("shoppingcarts", ShoppingCart.class, restClient, (m) -> m.getId());
    }
}
