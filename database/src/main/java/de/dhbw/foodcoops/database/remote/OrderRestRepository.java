package de.dhbw.foodcoops.database.remote;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IRestClient;
import de.dhbw.foodcoops.model.Order;
import java.util.List;

public class OrderRestRepository extends RestRepositoryBase<Order> {

    public OrderRestRepository(IRestClient restclient) {
        super("orders", Order.class, restclient, (m) -> m.getId());
    }
}
