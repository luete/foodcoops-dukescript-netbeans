package de.dhbw.foodcoops.database.impl;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.inmemory.InMemoryRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ProductOffer;
import java.util.ArrayList;
import java.util.List;

public class ProductOfferInMemoryRepository implements IRepository<ProductOffer> {

    private final IRepository<Product> productRepository;
    private final IRepository<ProductOffer> offerRepository;

    public ProductOfferInMemoryRepository(IRepository<Product> productRepository) {
        this.productRepository = productRepository;
        this.offerRepository = new InMemoryRepository<>((m) -> m.getId(), (id, m) -> m.setId(id));
    }

    @Override
    public void getAll(IResultHandler<List<ProductOffer>> resultHandler) {
        offerRepository.getAll(new IResultHandler<List<ProductOffer>>() {
            @Override
            public void onSuccess(List<ProductOffer> allOffers) {
                List<ProductOffer> result = new ArrayList<>();
                for (ProductOffer offer : allOffers) {
                    findById(offer.getId(), new IResultHandler<ProductOffer>() {
                        @Override
                        public void onSuccess(ProductOffer convertedOffer) {
                            result.add(convertedOffer);
                        }

                        @Override
                        public void onError(Exception ex) {
                            resultHandler.onError(ex);
                        }
                    });
                }
                
                resultHandler.onSuccess(result);
            }

            @Override
            public void onError(Exception ex) {
                resultHandler.onError(ex);
            }
        });
    }

    @Override
    public void add(ProductOffer obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        offerRepository.add(obj, resultHandler);
    }

    @Override
    public void remove(ProductOffer obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        offerRepository.remove(obj, resultHandler);
    }

    @Override
    public void update(ProductOffer obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        offerRepository.update(obj, resultHandler);
    }

    @Override
    public void findById(int id, IResultHandler<ProductOffer> resultHandler) {
        offerRepository.findById(id, new IResultHandler<ProductOffer>() {
            @Override
            public void onSuccess(ProductOffer offerResult) {
                productRepository.findById(offerResult.getProduct().getId(), new IResultHandler<Product>(){
                    @Override
                    public void onSuccess(Product productResult) {
                        offerResult.setProduct(productResult);
                        resultHandler.onSuccess(offerResult);
                    }

                    @Override
                    public void onError(Exception ex) {
                        resultHandler.onError(ex);
                    }
                });
            }

            @Override
            public void onError(Exception ex) {
                resultHandler.onError(ex);
            }
        });
    }
}
