package de.dhbw.foodcoops.database;

import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.model.Extraction;
import de.dhbw.foodcoops.database.resulthandling.Void;

public interface IExtractionRepository extends IRepository<Extraction> {
    void pay(int id, IResultHandler<Void> resultHandler);
}
