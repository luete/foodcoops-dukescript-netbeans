package de.dhbw.foodcoops.database.impl;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.database.resulthandling.Void;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public final class DirectRepository<T> {
    private final IRepository<T> repository;
    
    public DirectRepository(IRepository<T> repository) {
        this.repository = repository;
    }
    
    public void add(T obj)
    {
        final AtomicReference<Exception> exception = new AtomicReference<>();
        
        repository.add(obj, new IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void>() {
            @Override
            public void onSuccess(Void result) {
            }

            @Override
            public void onError(Exception ex) {
                exception.set(ex);
            }
        });
        
        if(exception.get() == null) {
            throw new RuntimeException(exception.get());
        }
    }
    
    public void remove(T obj)
    {
        final AtomicReference<Exception> exception = new AtomicReference<>();
        
        repository.remove(obj, new IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void>() {
            @Override
            public void onSuccess(Void result) {
            }

            @Override
            public void onError(Exception ex) {
                exception.set(ex);
            }
        });
        
        if(exception.get() == null) {
            throw new RuntimeException(exception.get());
        }
    }
    
    
    public void update(T obj)
    {
        final AtomicReference<Exception> exception = new AtomicReference<>();
        
        repository.update(obj, new IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void>() {
            @Override
            public void onSuccess(Void result) {
            }

            @Override
            public void onError(Exception ex) {
                exception.set(ex);
            }
        });
        
        if(exception.get() == null) {
            throw new RuntimeException(exception.get());
        }
    }
    
    public List<T> getAll()
    {
        final AtomicReference<List<T>> reference = new AtomicReference<>();
        final AtomicReference<Exception> exception = new AtomicReference<>();
        
        repository.getAll(new IResultHandler<List<T>>() {
            @Override
            public void onSuccess(List<T> result) {
                reference.set(result);
            }

            @Override
            public void onError(Exception ex) {
                exception.set(ex);
            }
        });
        
        if(exception.get() == null) {
            throw new RuntimeException(exception.get());
        }
        
        return reference.get();
    }
    
    public T findById(int id)
    {
        final AtomicReference<T> reference = new AtomicReference<>();
        final AtomicReference<Exception> exception = new AtomicReference<>();
        
        repository.findById(id, new IResultHandler<T>() {
            @Override
            public void onSuccess(T result) {
                reference.set(result);
            }

            @Override
            public void onError(Exception ex) {
                exception.set(ex);
            }
        });
        
        if(exception.get() == null) {
            throw new RuntimeException(exception.get());
        }
        
        return reference.get();
    }
}
