package de.dhbw.foodcoops.database.resulthandling;

/**
 * ResultHandler that accepts delegates for each operation to enable use of lambdas.
 */
public final class DelegateResultHandler<T> implements IResultHandler<T> {
    private final IParameterizedAction<T> successAction;
    private final IParameterizedAction<Exception> failureAction;

    public DelegateResultHandler(IParameterizedAction<T> successAction, IParameterizedAction<Exception> failureAction) {
        this.successAction = successAction;
        this.failureAction = failureAction;
    }

    @Override
    public void onSuccess(T result) {
        successAction.perform(result);
    }

    @Override
    public void onError(Exception ex) {
        failureAction.perform(ex);
    }
}
