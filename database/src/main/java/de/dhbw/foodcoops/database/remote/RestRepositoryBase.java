package de.dhbw.foodcoops.database.remote;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.inmemory.IIdentityProvider;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import net.java.html.BrwsrCtx;
import net.java.html.json.Models;

public abstract class RestRepositoryBase<TManagedEntity> implements IRepository<TManagedEntity> {

    private final String resourceName;
    private final Class<TManagedEntity> managedEntityClass;
    private final IIdentityProvider identityProvider;
    protected final IRestClient restClient;

    public RestRepositoryBase(String resourceName, Class<TManagedEntity> managedEntityClass, IRestClient restClient, IIdentityProvider<TManagedEntity> identityProvider) {
        this.resourceName = resourceName;
        this.managedEntityClass = managedEntityClass;
        this.restClient = restClient;
        this.identityProvider = identityProvider;
    }

    @Override
    public void remove(TManagedEntity obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        restClient.sendAsync(createSubResourceUrl(String.valueOf(identityProvider.idFor(obj))),
                "DELETE", "",
                new IRestCallback() {
            @Override
            public void onSuccess(int statusCode, String responseText) {
                resultHandler.onSuccess(de.dhbw.foodcoops.database.resulthandling.Void.INSTANCE);
            }

            @Override
            public void onError(String errorText) {
                resultHandler.onError(new Exception(errorText));
            }
        });
    }

    @Override
    public void add(TManagedEntity obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        restClient.sendAsync(createCollectionResourceUrl(), "POST", obj.toString(), new IRestCallback() {
            @Override
            public void onSuccess(int statusCode, String responseText) {
                resultHandler.onSuccess(de.dhbw.foodcoops.database.resulthandling.Void.INSTANCE);
            }

            @Override
            public void onError(String errorText) {
                resultHandler.onError(new Exception(errorText));
            }
        });
    }

    @Override
    public void update(TManagedEntity obj, IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler) {
        String url = createSubResourceUrl(String.valueOf(identityProvider.idFor(obj)));
        restClient.sendAsync(url, "PUT", obj.toString(), new IRestCallback() {
            @Override
            public void onSuccess(int statusCode, String responseText) {
                resultHandler.onSuccess(de.dhbw.foodcoops.database.resulthandling.Void.INSTANCE);
            }

            @Override
            public void onError(String errorText) {
                resultHandler.onError(new Exception(errorText));
            }
        });
    }

    @Override
    public void getAll(IResultHandler<List<TManagedEntity>> resultHandler) {
        restClient.sendAsync(createCollectionResourceUrl(), "GET", null, new IRestCallback() {
            @Override
            public void onSuccess(int statusCode, String result) {
                parseList(result, managedEntityClass, resultHandler);
            }

            @Override
            public void onError(String ex) {
                resultHandler.onError(new Exception(ex));
            }
        });
    }

    @Override
    public void findById(int id, IResultHandler<TManagedEntity> resultHandler) {
        restClient.send(createSubResourceUrl(String.valueOf(id)), "GET", null, new IRestCallback()
        {
            @Override
            public void onSuccess(int statusCode, String responseText) {
                parseModel(responseText, managedEntityClass, resultHandler);
            }

            @Override
            public void onError(String errorText) {
                resultHandler.onError(new Exception(errorText));
            }
        });
    }

    protected String createCollectionResourceUrl() {
        return resourceName;
    }

    protected String createSubResourceUrl(String subPath) {
        return createCollectionResourceUrl() + "/" + subPath;
    }

    protected <X> void parseList(String json, Class<X> modelClass, IResultHandler<List<X>> resultHandler) {
        try {
            List<X> list = new ArrayList();
            InputStream is = new ByteArrayInputStream(json.getBytes());
            Models.parse(parsingContext(), modelClass, is, list);
            resultHandler.onSuccess(list);
        } catch (Exception ex) {
            resultHandler.onError(ex);
        }
    }

    protected final <X> void parseModel(String json, Class<X> modelClass, IResultHandler<X> resultHandler) {
        try {
            InputStream is = new ByteArrayInputStream(json.getBytes());
            X model = Models.parse(parsingContext(), modelClass, is);
            resultHandler.onSuccess(model);
        } catch (IOException ex) {
            resultHandler.onError(ex);
        }
    }

    protected static BrwsrCtx parsingContext() {
        return BrwsrCtx.findDefault(RestRepositoryBase.class);
    }
}
