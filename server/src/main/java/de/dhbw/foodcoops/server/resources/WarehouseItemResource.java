package de.dhbw.foodcoops.server.resources;

import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.WarehouseItem;
import de.dhbw.foodcoops.server.AuthorizationUtils;
import de.dhbw.foodcoops.server.ResponseFactory;
import static de.dhbw.foodcoops.server.ResponseFactory.*;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("warehouseitems")
public class WarehouseItemResource extends BasicResource<WarehouseItem> {

    public WarehouseItemResource() {
        super("WarehouseItem", WarehouseItem.class);
    }

    @OPTIONS
    public Response preflightRequest_Collection() {
        return super.preflightRequest_Collection();
    }

    @OPTIONS
    @Path("/{id}")
    public Response preflightRequest_Resource() {
        return super.preflightRequest_Resource();
    }

    @GET
    public Response getAllResponse(@HeaderParam("Authorization") String authorizationHeader) {
        if (!AuthorizationUtils.isMember(authorizationHeader)) {
            return forbiddenStatus("Please provide valid credentials");
        }

        return super.getAllResponse();
    }

    @GET
    @Path("/{id}")
    public Response findById(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isMember(authorizationHeader)) {
            return forbiddenStatus("Please provide valid credentials");
        }

        return super.findById(id);
    }

    @POST
    public Response addItem(@HeaderParam("Authorization") String authorizationHeader,
            WarehouseItem itemToAdd) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("This resource can only be modified by admins.");
        }

        return performOperation(entitymanager -> {
            try {
                int offerId = itemToAdd.getOffer().getId();
                List<WarehouseItem> existingItems = entitymanager.createQuery("SELECT i FROM WarehouseItem i WHERE i.offer.id=" + offerId)
                        .getResultList();

                boolean warehouseItemWithSameOfferExists = existingItems.size() > 0;
                if (warehouseItemWithSameOfferExists) {
                    WarehouseItem existingItemToUpdate = existingItems.get(0);
                    if (existingItemToUpdate.getAmount() > 0) {
                        return conflictResponse("Es existiert bereits ein Lagerelement für dieses Produkt.");
                    } else {
                        entitymanager.getTransaction().begin();
                        
                        int newAmount = itemToAdd.getAmount() + existingItemToUpdate.getAmount();
                        existingItemToUpdate.setAmount(newAmount);
                        entitymanager.merge(existingItemToUpdate);
                        entitymanager.getTransaction().commit();

                        return okResponse();
                    }
                } else {
                    entitymanager.getTransaction().begin();
                    entitymanager.persist(itemToAdd);
                    entitymanager.getTransaction().commit();

                    return okResponse();
                }
            } catch (Exception e) {
                return serverErrorResponse();
            }
        });

    }

    @DELETE
    @Path("/{id}")
    public Response removeItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("This resource can only be modified by admins.");
        }

        return performOperation(entityManager -> {
            try {
                WarehouseItem item = entityManager.find(WarehouseItem.class, id);

                entityManager.getTransaction().begin();
                item.setAmount(0);
                entityManager.merge(item);
                entityManager.getTransaction().commit();

                return okResponse();
            } catch (Exception ex) {
                return serverErrorResponse(ex.getMessage());
            }
        });
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id,
            String newValues) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("This resource can only be modified by admins.");
        }

        return super.updateItem(newValues);
    }
}
