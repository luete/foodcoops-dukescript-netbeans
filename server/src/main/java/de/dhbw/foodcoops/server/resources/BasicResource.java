package de.dhbw.foodcoops.server.resources;

import static de.dhbw.foodcoops.server.ResponseFactory.okResponse;
import static de.dhbw.foodcoops.server.ResponseFactory.serverErrorResponse;
import java.util.List;
import java.util.function.Function;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.ws.rs.core.Response;

public abstract class BasicResource<TEntity> {

    private final String entityName;
    private final Class<TEntity> entityType;

    public BasicResource(String entityName, Class<TEntity> entityType) {
        this.entityName = entityName;
        this.entityType = entityType;
    }

    protected Response preflightRequest_Collection() {
        System.out.println("Preflight {resource}Collection");
        return okResponse();
    }

    protected Response preflightRequest_Resource() {
        System.out.println("Preflight {resource}/{id}");
        return okResponse();
    }

    protected Response getAllResponse() {
        try {
            return okResponse(getAll());
        } catch (Exception ex) {
            return serverErrorResponse(ex.getMessage());
        }
    }

    protected List<TEntity> getAll() {
        return performOperation(entitymanager -> {
            Query query = entitymanager.createQuery("Select c from " + entityName + " c");
            return query.getResultList();
        });
    }

    protected Response findById(int id) {
        try {
            TEntity entity = performOperation(entityManager -> {
                return entityManager.find(entityType, id);
            });

            return okResponse(entity);
        } catch (Exception ex) {
            return serverErrorResponse();
        }
    }

    protected Response add(String itemToAddJson) {
        TEntity entityToAdd = parseModel(itemToAddJson);

        return add(entityToAdd);
    }

    protected Response add(TEntity c) {
        try {
            performOperation(entitymanager -> {
                entitymanager.getTransaction().begin();
                entitymanager.persist(c);
                entitymanager.getTransaction().commit();
                return null;
            });

            return okResponse();
        } catch (Exception e) {
            return serverErrorResponse();
        }
    }

    protected Response removeItem(int id) {
        try {
            performOperation(entitymanager -> {
                entitymanager.getTransaction().begin();

                TEntity itemToRemove = entitymanager.find(entityType, id);
                entitymanager.remove(itemToRemove);

                entitymanager.getTransaction().commit();
                return null;
            });

            return okResponse();
        } catch (Exception ex) {
            return serverErrorResponse();
        }
    }

    protected Response updateItem(String itemToUpdate) {
        return performOperation(entityManager -> {
            try {
                entityManager.getTransaction().begin();
                entityManager.merge(parseModel(itemToUpdate));
                entityManager.getTransaction().commit();
            } catch (Exception e) {
                return serverErrorResponse(e.getMessage());
            }

            return okResponse();
        });
    }

    protected <T> T performOperation(Function<EntityManager, T> funcToPerform) {
        EntityManagerFactory emfactory = null;
        EntityManager entitymanager = null;
        try {
            emfactory = Persistence.createEntityManagerFactory("FoodCoops-ServerDB");
            entitymanager = emfactory.createEntityManager();

            return funcToPerform.apply(entitymanager);
        } catch (Exception ex) {
            if (entitymanager != null && entitymanager.getTransaction().isActive()) {
                entitymanager.getTransaction().rollback();
            }
            throw ex;
        } finally {
            if (entitymanager != null) {
                entitymanager.close();
            }
            if (emfactory != null) {
                emfactory.close();
            }
        }
    }

    protected final <X> X parseModel(String json, Class<X> modelClass) {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper().readValue(json, modelClass);
        } catch (Exception ex) {
            throw new RuntimeException();
        }
    }

    protected final TEntity parseModel(String json) {
        return parseModel(json, entityType);
    }
}
