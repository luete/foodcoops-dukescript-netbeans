package de.dhbw.foodcoops.server.resources;

import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.server.AuthorizationUtils;
import static de.dhbw.foodcoops.server.ResponseFactory.*;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("offers")
public class ProductOfferResource extends BasicResource<ProductOffer> {

    public ProductOfferResource() {
        super("ProductOffer", ProductOffer.class);
    }

    @OPTIONS
    public Response preflightRequest_Collection() {
        System.out.println("Preflight ProductCollection");
        return okResponse();
    }

    @OPTIONS
    @Path("/{id}")
    public Response preflightRequest_Resource() {
        System.out.println("Preflight offers/{id}");
        return okResponse();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllResponse(@HeaderParam("Authorization") String authorizationHeader) {
        if(!AuthorizationUtils.isMember(authorizationHeader)) {
            return forbiddenStatus("Ungültige Anmeldeinformationen");
        }
        
        List<ProductOffer> allCurrentOffers = super.getAll()
                .stream()
                .filter(o -> false == o.isOutdated())
                .collect(Collectors.toList());
        
        return okResponse(allCurrentOffers);
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@HeaderParam("Authorization") String authorizationHeader,
                            @PathParam("id") int id) {
        if(!AuthorizationUtils.isMember(authorizationHeader)) {
            return forbiddenStatus("Ungültige Anmeldeinformationen");
        }
        return super.findById(id);
    }

    @POST
    public Response addItem(ProductOffer itemToAdd) {
        return forbiddenStatus("This resource can only be modified by the server itself.");
    }

    @DELETE
    @Path("/{id}")
    public Response removeItem(@PathParam("id") String id) {
        return forbiddenStatus("This resource can only be modified by the server itself.");
    }

    @PUT
    @Path("/{id}")
    public Response updateItem(ProductOffer itemToUpdate) {
        return forbiddenStatus("This resource can only be modified by the server itself.");
    }
}
