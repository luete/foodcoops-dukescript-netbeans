package de.dhbw.foodcoops.server.resources;

import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Order;
import de.dhbw.foodcoops.server.AuthorizationUtils;
import static de.dhbw.foodcoops.server.ResponseFactory.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("orders")
public class OrderResource extends BasicResource<Order> {

    public OrderResource() {
        super("Order", Order.class);
    }

    @OPTIONS
    public Response preflightRequest_Collection() {
        return super.preflightRequest_Collection();
    }

    @OPTIONS
    @Path("/{id}")
    public Response preflightRequest_Resource() {
        return super.preflightRequest_Resource();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@HeaderParam("FC-App") String appId,
            @HeaderParam("Authorization") String authorizationHeader) {
        Member member = AuthorizationUtils.memberForCredentials(authorizationHeader);

        if (member == null) {
            return forbiddenStatus("Ungültige Anmeldeinformationen");
        }

        if ("AdminApp".equals(appId)) {
            if (member.isAdmin()) {
                return super.getAllResponse();
            } else {
                return forbiddenStatus("You need to be admin.");
            }
        } else if ("UserApp".equals(appId)) {
            return performOperation(manager -> {
                Object[] result = super.getAll().stream()
                        .filter(order -> member.getId() == order.getUser().getId())
                        .toArray();
                return okResponse(result);
            });
        }

        return serverErrorResponse("Invalid AppId");
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("Only admins can access this resource");
        }

        return super.findById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addItem(@HeaderParam("FC-App") String appId,
            @HeaderParam("Authorization") String authorizationHeader,
            String itemToAddJson) {
        Member member = AuthorizationUtils.memberForCredentials(authorizationHeader);

        if (member == null) {
            return forbiddenStatus("Ungültige Anmeldeinformationen");
        }

        if ("AdminApp".equals(appId)) {
            if (member.isAdmin()) {
                Order order = parseModel(itemToAddJson);
                order.setUser(member);
                
                return performOperation(entityManager -> {
                    entityManager.getTransaction().begin();

                    entityManager.persist(order);
                    
                    entityManager.getTransaction().commit();
                    return okResponse();
                });
            } else {
                return forbiddenStatus("You need to be admin.");
            }
        }

        return serverErrorResponse("Invalid AppId");
    }

    @DELETE
    @Path("/{id}")
    public Response removeItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("Only admins can access this resource");
        }

        return super.removeItem(id);
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id,
            String itemToUpdate) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("You need to be admin");
        }

        return super.updateItem(itemToUpdate);
    }
}
