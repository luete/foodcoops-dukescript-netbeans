package de.dhbw.foodcoops.server.resources;

import de.dhbw.foodcoops.model.IncomingGoods;
import de.dhbw.foodcoops.model.IncomingItem;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.WarehouseItem;
import de.dhbw.foodcoops.server.AuthorizationUtils;
import static de.dhbw.foodcoops.server.ResponseFactory.*;
import java.util.List;
import java.util.Optional;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("incominggoods")
public class IncomingGoodsResource extends BasicResource<IncomingGoods> {

    public IncomingGoodsResource() {
        super("IncomingGoods", IncomingGoods.class);
    }

    @OPTIONS
    public Response preflightRequest_Collection() {
        return super.preflightRequest_Collection();
    }

    @OPTIONS
    @Path("/{id}")
    public Response preflightRequest_Resource() {
        return super.preflightRequest_Resource();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@HeaderParam("FC-App") String appId,
            @HeaderParam("Authorization") String authorizationHeader) {
        Member member = AuthorizationUtils.memberForCredentials(authorizationHeader);

        if (member == null) {
            return forbiddenStatus("Please provide credentials.");
        }

        switch (appId) {
            case "AdminApp":
                return getAllAdminApp(member);
            case "UserApp":
                return getAllUserApp(member);
            default:
                return serverErrorResponse("Invalid AppId");
        }
    }

    private Response getAllAdminApp(Member member) {
        if (member.isAdmin()) {
            return super.getAllResponse();
        } else {
            return forbiddenStatus("You need to be admin.");
        }
    }

    private Response getAllUserApp(Member member) {
        return performOperation(manager -> {
            Object[] result = super.getAll().stream()
                    .filter(goods -> member.getId() == goods.getContactPerson().getId())
                    .toArray();
            return okResponse(result);
        });
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isMember(authorizationHeader)) {
            return forbiddenStatus("Please provide valid credentials");
        }

        return super.findById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addItem(@HeaderParam("Authorization") String authorizationHeader,
            String itemToAddJson) {
        if (!AuthorizationUtils.isMember(authorizationHeader)) {
            return forbiddenStatus("Please provide valid credentials");
        }

        performOperation(entityManager -> {
            entityManager.getTransaction().begin();

            IncomingGoods goods = parseModel(itemToAddJson);
            entityManager.persist(goods);

            Query query = entityManager.createQuery("Select c from WarehouseItem c");
            List<WarehouseItem> allWarehouseItems = query.getResultList();
            List<IncomingItem> newWarehouseItems = goods.getIncomingItems();
            for (IncomingItem newItem : newWarehouseItems) {
                Optional<WarehouseItem> existingItemToUpdate = allWarehouseItems.stream()
                        .filter(i -> i.getOffer().getId() == newItem.getOffer().getId())
                        .findFirst();

                if (existingItemToUpdate.isPresent()) {
                    int originalAmount = existingItemToUpdate.get().getAmount();
                    existingItemToUpdate.get().setAmount(originalAmount + newItem.getAmount());
                } else {
                    entityManager.persist(new WarehouseItem(0, newItem.getOffer(), newItem.getAmount()));
                }
            }

            entityManager.getTransaction().commit();

            return null;
        });
        
        return okResponse();
    }

    @DELETE
    @Path("/{id}")
    public Response removeItem(@HeaderParam("FC-App") String appId,
            @HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        Member member = AuthorizationUtils.memberForCredentials(authorizationHeader);

        if (member == null) {
            return forbiddenStatus("Please provide credentials.");
        }

        switch (appId) {
            case "AdminApp":
                return removeAdminApp(id, member);
            case "UserApp":
                return removeUserApp(id, member);
            default:
                return serverErrorResponse("Invalid AppId");
        }
    }

    private Response removeAdminApp(int id, Member member) {
        if (member.isAdmin()) {
            return super.removeItem(id);
        }
        return forbiddenStatus("You need to be admin.");
    }

    private Response removeUserApp(int id, Member member) {
        return performOperation(manager -> {
            IncomingGoods goods = manager.find(IncomingGoods.class, id);
            if (goods.getContactPerson().getId() == member.getId()) {
                return super.removeItem(id);
            }
            return forbiddenStatus("Not your resource");
        });
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateItem(@HeaderParam("FC-App") String appId,
            @HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id,
            String itemToUpdate) {
        return forbiddenStatus("Can't manipulate this resource");
    }
}
