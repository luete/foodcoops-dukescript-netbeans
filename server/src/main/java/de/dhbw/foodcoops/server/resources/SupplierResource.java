package de.dhbw.foodcoops.server.resources;

import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.Supplier;
import de.dhbw.foodcoops.server.AuthorizationUtils;
import static de.dhbw.foodcoops.server.ResponseFactory.*;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("suppliers")
public class SupplierResource extends BasicResource<Supplier> {

    public SupplierResource() {
        super("Supplier", Supplier.class);
    }

    @OPTIONS
    public Response preflightRequest_Collection() {
        return super.preflightRequest_Collection();
    }

    @OPTIONS
    @Path("/{id}")
    public Response preflightRequest_Resource() {
        return super.preflightRequest_Resource();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllResponse(@HeaderParam("Authorization") String authorizationHeader) {
        if (!AuthorizationUtils.isMember(authorizationHeader)) {
            return forbiddenStatus("Please provide valid credentials");
        }
        
        return super.getAllResponse();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findById(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isMember(authorizationHeader)) {
            return forbiddenStatus("Please provide valid credentials");
        }

        return super.findById(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addItem(@HeaderParam("Authorization") String authorizationHeader,
            String itemToAddJson) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("Only admins can access this resource");
        }

        try {
            performOperation(entitymanager -> {
                Supplier itemToAdd = parseModel(itemToAddJson);

                entitymanager.getTransaction().begin();
                entitymanager.persist(itemToAdd);

                for (ProductOffer productOffer : itemToAdd.getProductOffers()) {
                    productOffer.setSupplierId(itemToAdd.getId());
                }

                entitymanager.getTransaction().commit();
                return null;
            });

            return okResponse();
        } catch (Exception e) {
            return serverErrorResponse();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response removeItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("Only admins can access this resource");
        }

        performOperation(entityManager -> {
            entityManager.getTransaction().begin();

            Supplier supplierToRemove = entityManager.find(Supplier.class, id);
            supplierToRemove.setOutdated(true);
            supplierToRemove.getProductOffers().forEach(offer -> offer.setOutdated(true));

            entityManager.merge(supplierToRemove);

            entityManager.getTransaction().commit();
            return null;
        });

        return super.removeItem(id);
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateItem(@HeaderParam("Authorization") String authorizationHeader,
            @PathParam("id") int id,
            String newValuesJson) {
        if (!AuthorizationUtils.isAdmin(authorizationHeader)) {
            return forbiddenStatus("This resource can only be modified by admins.");
        }

        Supplier newValues = parseModel(newValuesJson);
        performOperation(entityManager -> {
            entityManager.getTransaction().begin();

            Supplier originalSupplier = entityManager.find(Supplier.class, id);
            originalSupplier.getProductOffers()
                    .stream()
                    .filter(o -> false == newValues.getProductOffers().contains(o))
                    .forEach(o -> o.setOutdated(true));

            entityManager.merge(newValues);

            entityManager.getTransaction().commit();

            return null;
        });

        return okResponse();
    }
}
