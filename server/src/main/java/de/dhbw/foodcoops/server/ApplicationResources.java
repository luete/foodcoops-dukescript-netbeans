package de.dhbw.foodcoops.server;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.Produces;

@ApplicationScoped
public class ApplicationResources {
//    @Default 
//    @PersistenceContext(unitName="FoodCoops-ServerDB")
    private EntityManagerFactory entityManager;
    
    @Produces
//    @RequestScoped
    public EntityManager entityManager(){
        return entityManager.createEntityManager();
    }    
}