package de.dhbw.foodcoops.viewmodel.shared;

import de.dhbw.foodcoops.client.resulthandler.decorator.ResultHandlerToastLogDecorator;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.log.ILogger;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Order;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.ShoppingCart;
import de.dhbw.foodcoops.viewmodel.shared.ShoppingCartManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.shared.ShoppingCartRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "ShoppingCartRootViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "shoppingCartManipulation", type = ShoppingCartManipulationViewModel.class),
            @Property(name = "shoppingCartDetails", type = ShoppingCart.class),
            @Property(name = "shoppingCartList", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class),
            @Property(name = "adminApp", type = boolean.class)
        })
public class ShoppingCartRootViewModelDef {

    private IPlatformServices services;
    private ILogger logger;
    private IRepository<ShoppingCart> shoppingCartRepository;
    private IRepository<Order> orderRepository;
    private IRepository<ProductOffer> productOfferRepository;
    private IRepository<Member> memberRepository;

    public static ShoppingCartRootViewModel create(IPlatformServices services, ILogger logger,
            IRepository<ShoppingCart> shoppingCartRepository,
            IRepository<ProductOffer> productOfferRepository,
            IRepository<Member> memberRepository,
            IRepository<Order> orderRepository) {
        ShoppingCartRootViewModel vm = new ShoppingCartRootViewModel();

        vm.setLoading(true);
        vm.initServices(services);
        vm.initLogger(logger);
        vm.initShoppingCartRepository(shoppingCartRepository);
        vm.initProductOfferRepository(productOfferRepository);
        vm.initMemberRepository(memberRepository);
        vm.initOrderRepository(orderRepository);

        boolean isAdminApp = services.getPreferences("app.id", "").equals("AdminApp");
        vm.setAdminApp(isAdminApp);

        ListViewModel shoppingCartList = new ListViewModel();
        vm.setShoppingCartList(shoppingCartList);
        vm.setShoppingCartDetails(new ShoppingCart());

        return vm;
    }

    @ModelOperation
    public void initShoppingCartRepository(ShoppingCartRootViewModel model, IRepository<ShoppingCart> repository) {
        this.shoppingCartRepository = repository;
    }

    @ModelOperation
    public void initProductOfferRepository(ShoppingCartRootViewModel model, IRepository<ProductOffer> repository) {
        this.productOfferRepository = repository;
    }

    @ModelOperation
    public void initMemberRepository(ShoppingCartRootViewModel model, IRepository<Member> repository) {
        this.memberRepository = repository;
    }

    @ModelOperation
    public void initOrderRepository(ShoppingCartRootViewModel model, IRepository<Order> repository) {
        this.orderRepository = repository;
    }

    @ModelOperation
    public void initServices(ShoppingCartRootViewModel model, IPlatformServices services) {
        this.services = services;

        model.getShoppingCartList().initServices(services);
    }

    @ModelOperation
    public void initLogger(ShoppingCartRootViewModel model, ILogger logger) {
        this.logger = logger;
    }

    @Function
    public void navigateToAddPage(ShoppingCartRootViewModel model) {
        ISaveAction<ShoppingCart> saveAction = (newShoppingCart) -> {
            IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> saveResultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Einkaufswagen erfolgreich gespeichert", "Fehler beim Speichern des Einkaufswagens",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            shoppingCartRepository.add(newShoppingCart, saveResultHandler);
        };

        ShoppingCartManipulationViewModel manipulationVM = ShoppingCartManipulationViewModelDef.createAddPage(services, saveAction, productOfferRepository, memberRepository);
        model.setShoppingCartManipulation(manipulationVM);

        OnsenUITemplates.switchPage("shared/shoppingCart/manipulate.html");
    }

    private static void refreshAndGoToHome(ShoppingCartRootViewModel model, IPlatformServices services) {
        model.refreshShoppingCartList();
        OnsenUITemplates.goBackOnePage();
    }

    @ModelOperation
    public void navigateToEditPage(ShoppingCartRootViewModel model, ShoppingCart shoppingCartToEdit) {
        ISaveAction<ShoppingCart> saveAction = (editedShoppingCart) -> {
            IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Einkaufswagen erfolgreich gespeichert", "Fehler beim Speichern des Einkaufswagens",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            shoppingCartRepository.update(editedShoppingCart, resultHandler);
            refreshAndGoToHome(model, services);
        };

        ShoppingCartManipulationViewModel manipulationVM = ShoppingCartManipulationViewModelDef.createEditPage(shoppingCartToEdit.clone(), services, saveAction, productOfferRepository, memberRepository);
        model.setShoppingCartManipulation(manipulationVM);

        OnsenUITemplates.switchPage("shared/shoppingCart/manipulate.html");
    }

    @Function
    @ModelOperation
    public void refreshShoppingCartList(ShoppingCartRootViewModel model) {
        model.setLoading(true);

        IResultHandler<List<ShoppingCart>> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                "Einkaufswagen-Liste erfolgreich geladen", "Fehler beim Laden der Einkaufswagen-Liste",
                (result) -> {
                    model.getShoppingCartList().getItems().clear();
                    for (ShoppingCart shoppingCart : result) {
                        ListItemViewModel listItem = createListItemFor(model, shoppingCartRepository, orderRepository, services, logger, shoppingCart);
                        model.getShoppingCartList().getItems().add(listItem);
                    }

                    model.setLoading(false);
                },
                (exception) -> {
                    model.setLoading(false);
                });

        shoppingCartRepository.getAll(resultHandler);
    }

    @ModelOperation
    public void navigateToDetailsPage(ShoppingCartRootViewModel model, ShoppingCart shoppingCart) {
        model.setShoppingCartDetails(shoppingCart);
        OnsenUITemplates.switchPage("shared/shoppingCart/details.html");
    }

    private static ListItemViewModel createListItemFor(ShoppingCartRootViewModel model, IRepository<ShoppingCart> shoppingCartRepository, IRepository<Order> orderRepository, IPlatformServices services, ILogger logger, ShoppingCart shoppingCart) {
        String itemTitle = shoppingCart.getId() + ": " + "Einkaufswagen";

        String itemDescription = shoppingCart.getItemCount() + " Produkt(e)";
        ListItemViewModel listItem = new ListItemViewModel(itemTitle, itemDescription);

        ItemAction editAction = new ItemAction("md-edit");
        editAction.init(() -> model.navigateToEditPage(shoppingCart));
        listItem.getActions().add(editAction);

        if (model.isAdminApp()) {
            ItemAction deleteAction = new ItemAction("md-delete");
            deleteAction.init(() -> {
                services.confirmByUser("Einkaufswagen wirklich löschen?", () -> {
                    IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                            "Einkaufswagen erfolgreich gelöscht", "Fehler beim Löschen des Einkaufswagens",
                            (result) -> model.refreshShoppingCartList(),
                            (exception) -> model.refreshShoppingCartList());

                    shoppingCartRepository.remove(shoppingCart, resultHandler);
                });
            });
            listItem.getActions().add(deleteAction);

            ItemAction sendAction = new ItemAction("md-mail-send");
            sendAction.init(() -> {
                if(!shoppingCart.isMinimumOrderQuantityReached()) {
                    services.alert("Einkaufswagen kann nicht in eine Bestellung umgewandelt werden.\nDie Mindestbestellmenge wurde noch nicht erreicht.");
                    return;
                }
                if(shoppingCart.isAnyOutdatedOfferReferenced()) {
                    services.alert("Einkaufswagen kann nicht in eine Bestellung umgewandelt werden.\nEr enthält veraltete Angebote.");
                    return;
                }
                
                services.confirmByUser("Bestellung wirklich versenden?", () -> {
                    Order order = new Order();
                    order.setShoppingCart(shoppingCart);

                    IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> addHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                            "Bestellung wurde erfolgreich versendet", "Fehler beim Senden der Bestellung",
                            (result) -> model.refreshShoppingCartList(),
                            (exception) -> model.refreshShoppingCartList());

                    orderRepository.add(order, addHandler);
                });
            });
            listItem.getActions().add(sendAction);
        }

        listItem.initDetailsAction(() -> model.navigateToDetailsPage(shoppingCart));

        return listItem;
    }
}
