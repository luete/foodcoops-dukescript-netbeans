package de.dhbw.foodcoops.viewmodel.app.admin;

import de.dhbw.foodcoops.client.resulthandler.decorator.ResultHandlerToastLogDecorator;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.log.ILogger;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.Order;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.Supplier;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.app.admin.OrderDetailsViewModel;
import de.dhbw.foodcoops.viewmodel.app.admin.OrderRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "OrderRootViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "orderDetails", type = OrderDetailsViewModel.class),
            @Property(name = "orderList", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class)
        })
public class OrderRootViewModelDef {

    private IPlatformServices services;
    private ILogger logger;
    private IRepository<Order> orderRepository;
    private IRepository<Supplier> supplierRepository;

    public static OrderRootViewModel create(IPlatformServices services, ILogger logger,
            IRepository<Order> orderRepository,
            IRepository<Supplier> supplierRepository) {
        OrderRootViewModel vm = new OrderRootViewModel();

        vm.setLoading(true);
        vm.initServices(services);
        vm.initLogger(logger);
        vm.initOrderRepository(orderRepository);
        vm.initSupplierRepository(supplierRepository);

        ListViewModel orderList = new ListViewModel();
        vm.setOrderList(orderList);
        vm.setOrderDetails(OrderDetailsViewModelDef.create(new Order(), supplierRepository, services));

        return vm;
    }

    @ModelOperation
    public void initOrderRepository(OrderRootViewModel model, IRepository<Order> repository) {
        this.orderRepository = repository;
    }

    @ModelOperation
    public void initSupplierRepository(OrderRootViewModel model, IRepository<Supplier> repository) {
        this.supplierRepository = repository;
    }

    @ModelOperation
    public void initServices(OrderRootViewModel model, IPlatformServices services) {
        this.services = services;

        model.getOrderList().initServices(services);
    }

    @ModelOperation
    public void initLogger(OrderRootViewModel model, ILogger logger) {
        this.logger = logger;
    }

    private static void refreshAndGoToHome(OrderRootViewModel model, IPlatformServices services) {
        model.refreshOrderList();
        OnsenUITemplates.goBackOnePage();
    }

    @Function
    @ModelOperation
    public void refreshOrderList(OrderRootViewModel model) {
        model.setLoading(true);

        IResultHandler<List<Order>> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                "Bestell-Liste erfolgreich geladen", "Fehler beim Laden der Bestell-Liste",
                (result) -> {
                    model.getOrderList().getItems().clear();
                    for (Order order : result) {
                        ListItemViewModel listItem = createListItemFor(model, orderRepository, services, logger, order);
                        model.getOrderList().getItems().add(listItem);
                    }

                    model.setLoading(false);
                },
                (exception) -> {
                    model.setLoading(false);
                });

        orderRepository.getAll(resultHandler);
    }

    @ModelOperation
    public void navigateToDetailsPage(OrderRootViewModel model, Order order) {
        model.setOrderDetails(OrderDetailsViewModelDef.create(order, supplierRepository, services));
        OnsenUITemplates.switchPage("administrationApp/order/details.html");
    }

    private static ListItemViewModel createListItemFor(OrderRootViewModel model, IRepository<Order> repository, IPlatformServices services, ILogger logger, Order order) {
        String itemTitle = order.getId() + ": " + order.getUser().getDisplayName();
        String itemDescription = order.getShoppingCart().getItemCount() + " Produkt(e)";
        ListItemViewModel listItem = new ListItemViewModel(itemTitle, itemDescription);

        ItemAction deleteAction = new ItemAction("md-delete");
        deleteAction.init(() -> {
            services.confirmByUser("Bestellung wirklich löschen?", () -> {
                IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                        "Bestellung erfolgreich gelöscht", "Fehler beim Löschen der Bestellung",
                        (result) -> model.refreshOrderList(),
                        (exception) -> model.refreshOrderList());

                repository.remove(order, resultHandler);
            });
        });
        listItem.getActions().add(deleteAction);

        listItem.initDetailsAction(() -> model.navigateToDetailsPage(order));

        return listItem;
    }
}
