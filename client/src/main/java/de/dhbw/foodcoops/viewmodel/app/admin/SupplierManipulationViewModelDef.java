package de.dhbw.foodcoops.viewmodel.app.admin;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.model.Product;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.ProductOfferDef;
import de.dhbw.foodcoops.model.Supplier;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.app.admin.SupplierManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.ArrayList;
import java.util.List;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "SupplierManipulationViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "pageTitle", type = String.class),
            @Property(name = "itemToManipulate", type = Supplier.class),
            @Property(name = "selectableProducts", type = de.dhbw.foodcoops.model.Product.class, array = true),
            @Property(name = "selectedProducts", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class),
            @Property(name = "selectedProductToAdd", type = de.dhbw.foodcoops.model.Product.class),
            @Property(name = "pricePerUnitToAdd", type = double.class),
            @Property(name = "minimumOrderQuantityToAdd", type = int.class)
        })
public class SupplierManipulationViewModelDef {

    private ISaveAction<Supplier> saveAction;
    private IPlatformServices services;

    public static SupplierManipulationViewModel createAddPage(IPlatformServices services, ISaveAction<Supplier> saveAction, IRepository<Product> productRepository) {
        return createPage(services, saveAction, productRepository, "Lieferant hinzufügen", new Supplier());
    }

    public static SupplierManipulationViewModel createEditPage(Supplier itemToManipulate, IPlatformServices services, ISaveAction<Supplier> saveAction, IRepository<Product> productRepository) {
        return createPage(services, saveAction, productRepository, "Lieferant bearbeiten", itemToManipulate);
    }

    private static SupplierManipulationViewModel createPage(IPlatformServices services, ISaveAction<Supplier> saveAction, IRepository<Product> productRepository, String pageTitle, Supplier itemToManipulate) {
        SupplierManipulationViewModel model = new SupplierManipulationViewModel();
        model.setLoading(true);
        model.setPageTitle(pageTitle);

        model.setItemToManipulate(itemToManipulate);
        model.setSelectedProducts(new ListViewModel());
        model.initServices(services);
        model.initSaveAction(saveAction);

        IResultHandler<List<Product>> getAllResultHandler = new DelegateResultHandler<>(
                (retrievedProducts) -> {
                    List<Product> selectableProducts = new ArrayList<>();
                    for (Product product : retrievedProducts) {
                        if(!product.isOutdated()) {
                            selectableProducts.add(product);
                        }
                    }
                    
                    model.getSelectableProducts().addAll(selectableProducts);
                    if (selectableProducts.size() > 0) {
                        model.setSelectedProductToAdd(selectableProducts.get(0));
                        model.setLoading(false);
                    } else {
                        services.toastWarning("Es wurden keine Produktangebote gefunden.");
                        OnsenUITemplates.goBackOnePage();
                    }
                },
                (exception) -> {
                    services.toastWarning("Produktangebote konnten nicht geladen werden: " + exception.getMessage());

                    OnsenUITemplates.goBackOnePage();
                    model.setLoading(false);
                });
        productRepository.getAll(getAllResultHandler);

        model.refreshProductList();

        return model;
    }

    @ModelOperation
    public void initSaveAction(SupplierManipulationViewModel model, ISaveAction<Supplier> saveAction) {
        this.saveAction = saveAction;
    }

    @ModelOperation
    public void initServices(SupplierManipulationViewModel model, IPlatformServices services) {
        this.services = services;
    }

    @Function
    public void save(SupplierManipulationViewModel model) {
        Supplier supplier = model.getItemToManipulate();

        if (!supplier.isValid()) {
            services.toastWarning("Please fill all form fields");
            return;
        }

        saveAction.save(supplier);
    }

    @ComputedProperty
    public static boolean productInputValid(double pricePerUnitToAdd) {
        return pricePerUnitToAdd > 0;
    }

    @ModelOperation
    public void refreshProductList(SupplierManipulationViewModel model) {
        ListViewModel productList = model.getSelectedProducts();
        productList.getItems().clear();

        List<ProductOffer> itemList = model.getItemToManipulate().getProductOffers();
        for (ProductOffer productOffer : itemList) {
            Product product = productOffer.getProduct();
            ListItemViewModel productItem = new ListItemViewModel(product.getName(), "Preis: " + productOffer.getPricePerUnit() + ", Mindestbestellmenge: " + productOffer.getMinimumOrderQuantity());
            ItemAction deleteAction = new ItemAction("md-delete");
            deleteAction.init(() -> {
                services.confirmByUser("Produktangebot wirklich löschen?", () -> {
                    itemList.remove(productOffer);
                    productList.getItems().remove(productItem);
                });
            });
            productItem.getActions().add(deleteAction);

            productList.getItems().add(productItem);
        }
    }

    @Function
    public void addProductOffer(SupplierManipulationViewModel model) {
        ProductOffer newProductOffer = ProductOfferDef.create(model.getSelectedProductToAdd(), model.getPricePerUnitToAdd(), model.getMinimumOrderQuantityToAdd());

        if (!newProductOffer.isValid()) {
            services.toastWarning("Bitte vervollständigen Sie die Angaben vor dem Hinzufügen!");
            return;
        }
        model.getItemToManipulate().getProductOffers().add(newProductOffer);
        model.refreshProductList();
    }
}
