package de.dhbw.foodcoops.viewmodel.shared;

/**
 * Functional interface used in any '...ManipulationViewModel'.
 * The interface implementation is responsible to save an entity, this may be for example an add or update operation.
 * @param <TSavedEntity> Type of the entity that should be saved.
 */
@FunctionalInterface
public interface ISaveAction<TSavedEntity> {
    void save(TSavedEntity parameter);
}
