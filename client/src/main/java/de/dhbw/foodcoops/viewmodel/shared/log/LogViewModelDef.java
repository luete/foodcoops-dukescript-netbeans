package de.dhbw.foodcoops.viewmodel.shared.log;

import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "LogViewModel", targetId = "", instance = true,
        properties = {
            @Property(name = "logEntries", type = de.dhbw.foodcoops.viewmodel.shared.log.LogEntryViewModel.class, array = true)
        })
public class LogViewModelDef {
    @Function
    public void clear(LogViewModel model)
    {
        model.getLogEntries().clear();
    }
}
