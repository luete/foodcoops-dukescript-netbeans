package de.dhbw.foodcoops.viewmodel.app.admin;

import de.dhbw.foodcoops.client.resulthandler.decorator.ResultHandlerToastLogDecorator;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.database.resulthandling.Void;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.log.ILogger;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.viewmodel.app.admin.MemberManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.app.admin.MemberRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import java.util.List;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "MemberRootViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "memberManipulation", type = MemberManipulationViewModel.class),
            @Property(name = "memberDetails", type = Member.class),
            @Property(name = "memberList", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class)
        })
public class MemberRootViewModelDef {

    private IRepository<Member> repository;
    private IPlatformServices services;
    private ILogger logger;

    public static MemberRootViewModel create(IPlatformServices services, ILogger logger, IRepository<Member> memberRepository) {
        MemberRootViewModel model = new MemberRootViewModel();

        model.setLoading(true);
        model.initServices(services);
        model.initDatabase(memberRepository);
        model.initLogger(logger);

        return model;
    }

    @ModelOperation
    public void initDatabase(MemberRootViewModel model, IRepository<Member> repository) {
        this.repository = repository;
    }

    @ModelOperation
    public void initServices(MemberRootViewModel model, IPlatformServices services) {
        this.services = services;

        model.getMemberList().initServices(services);
    }

    @ModelOperation
    public void initLogger(MemberRootViewModel model, ILogger logger) {
        this.logger = logger;
    }

    @Function
    public void navigateToAddPage(MemberRootViewModel model) {
        ISaveAction<Member> saveAction = (newMember) -> {
            IResultHandler<Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Mitglied erfolgreich gespeichert", "Fehler beim Speichern des Mitglieds",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            repository.add(newMember, resultHandler);
        };

        MemberManipulationViewModel manipulationVM = MemberManipulationViewModelDef.createAddPage(services, saveAction);
        model.setMemberManipulation(manipulationVM);

        OnsenUITemplates.switchPage("administrationApp/member/manipulate.html");
    }

    private static void refreshAndGoToHome(MemberRootViewModel model, IPlatformServices services) {
        model.refreshMemberList();
        OnsenUITemplates.goBackOnePage();
    }

    @ModelOperation
    public void navigateToEditPage(MemberRootViewModel model, Member memberToEdit) {
        ISaveAction<Member> saveAction = (editedMember) -> {
            IResultHandler<Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Mitglied erfolgreich gespeichert", "Fehler beim Speichern des Mitglieds",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            repository.update(editedMember, resultHandler);
            refreshAndGoToHome(model, services);
        };

        MemberManipulationViewModel manipulationVM = MemberManipulationViewModelDef.createEditPage(memberToEdit.clone(), services, saveAction);
        model.setMemberManipulation(manipulationVM);

        OnsenUITemplates.switchPage("administrationApp/member/manipulate.html");
    }

    @Function
    @ModelOperation
    public void refreshMemberList(MemberRootViewModel model) {
        model.setLoading(true);

        IResultHandler<List<Member>> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                "Mitglieder-Liste erfolgreich geladen", "Fehler beim Laden der Mitglieder-Liste",
                (result) -> {
                    model.getMemberList().getItems().clear();
                    for (Member member : result) {
                        ListItemViewModel listItem = createListItemFor(model, repository, services, logger, member);
                        model.getMemberList().addItem(listItem);
                    }

                    model.setLoading(false);
                },
                (exception) -> {
                    model.setLoading(false);
                });

        repository.getAll(resultHandler);
    }

    @ModelOperation
    public void navigateToDetailsPage(MemberRootViewModel model, Member member) {
        model.setMemberDetails(member);
        OnsenUITemplates.switchPage("administrationApp/member/details.html");
    }

    private static ListItemViewModel createListItemFor(MemberRootViewModel model, IRepository<Member> repository, IPlatformServices services, ILogger logger, Member member) {
        ListItemViewModel listItem = new ListItemViewModel(member.getId() + ": " + member.getDisplayName(),
                "Admin: " + member.isAdmin());

        ItemAction editAction = new ItemAction("md-edit");
        editAction.init(() -> model.navigateToEditPage(member));
        listItem.getActions().add(editAction);

        ItemAction deleteAction = new ItemAction("md-delete");
        deleteAction.init(() -> {
            String deleteMessage = "Mitglied wirklich löschen?\nAlle Einträge in aktiven Einkaufswägen werden dabei gelöscht.";
            services.confirmByUser(deleteMessage, () -> {
                IResultHandler<Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                        "Mitglied erfolgreich gelöscht", "Fehler beim Löschen des Mitglieds",
                        (result) -> model.refreshMemberList(),
                        (exception) -> model.refreshMemberList());

                repository.remove(member, resultHandler);
            });
        });
        listItem.getActions().add(deleteAction);

        listItem.initDetailsAction(() -> model.navigateToDetailsPage(member));

        return listItem;
    }
}
