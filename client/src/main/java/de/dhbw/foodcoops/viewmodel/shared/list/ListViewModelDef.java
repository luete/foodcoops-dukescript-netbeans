package de.dhbw.foodcoops.viewmodel.shared.list;

import de.dhbw.foodcoops.js.IPlatformServices;
import java.util.List;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.OnPropertyChange;
import net.java.html.json.Property;

@Model(className = "ListViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "filter", type = String.class),
            @Property(name = "items", type = ListItemViewModel.class, array = true),
            @Property(name = "filteredItems", type = ListItemViewModel.class, array = true)
        })
public class ListViewModelDef {
    private IPlatformServices services;

    @ModelOperation
    public void initServices(ListViewModel model, IPlatformServices services) {
        this.services = services;
    }
    
    @ModelOperation
    public void addItem(ListViewModel model, ListItemViewModel data){
        model.getItems().add(data);
        model.refresh();
    }

    @ComputedProperty
    public static int itemCount(List<ListItemViewModel> items) {
        return items.size();
    }
    
    @ComputedProperty
    public static int filteredItemCount(List<ListItemViewModel> filteredItems) {
        return filteredItems.size();
    }
    
    @OnPropertyChange("items")
    public void onItemsChange(ListViewModel model) {
        model.refresh();
    }
    
    @OnPropertyChange("filter")
    public void onFilterChange(ListViewModel model) {
        model.refresh();
    }

    @ModelOperation
    public void refresh(ListViewModel model) {
        try {
            model.getFilteredItems().clear();
            
            String filterText = model.getFilter();

            for (ListItemViewModel item : model.getItems()) {
                if(!doesFilterApply(item.getTitle(), filterText)
                   && !doesFilterApply(item.getDescription(), filterText)) {
                    continue;
                }

                model.getFilteredItems().add(item);
            }
        } catch (Exception ex) {
            services.alert("Exception(list.refresh): " + ex);
        }
    }
    
    private static boolean doesFilterApply(String itemText, String filterInput) {
        if(itemText == null) {
            return filterInput == null;
        }
        if(filterInput == null) {
            return true;
        }
        
        return itemText.toLowerCase().contains(filterInput.toLowerCase());
    }
}
