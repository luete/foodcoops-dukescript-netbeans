package de.dhbw.foodcoops.viewmodel.shared.list;

import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;


@Model(className = "ListItemViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "title", type = String.class),
            @Property(name = "description", type = String.class),
            @Property(name = "actions", type=ItemAction.class, array=true)
        })
public class ListItemViewModelDef {

    private Runnable detailsAction;

    @Function
    public void detailsClicked() {
        detailsAction.run();
    }
    
    @ModelOperation
    public void initDetailsAction(ListItemViewModel model, Runnable detailsAction) {
        this.detailsAction = detailsAction;
    }
}
