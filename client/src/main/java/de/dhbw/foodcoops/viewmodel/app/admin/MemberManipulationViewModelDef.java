package de.dhbw.foodcoops.viewmodel.app.admin;

import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.model.Member;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.ShoppingCart;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.app.admin.MemberManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.shared.ShoppingCartManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "MemberManipulationViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "pageTitle", type = String.class),
            @Property(name = "itemToManipulate", type = Member.class)
        })
public class MemberManipulationViewModelDef {

    private ISaveAction<Member> saveAction;
    private IPlatformServices services;

    public static MemberManipulationViewModel createAddPage(IPlatformServices services, ISaveAction<Member> saveAction) {
        return createPage(services, saveAction, "Mitglied hinzufügen", new Member());
    }

    public static MemberManipulationViewModel createEditPage(Member itemToManipulate, IPlatformServices services, ISaveAction<Member> saveAction) {
        return createPage(services, saveAction, "Mitglied bearbeiten", itemToManipulate);
    }

    private static MemberManipulationViewModel createPage(IPlatformServices services, ISaveAction<Member> saveAction, String pageTitle, Member itemToManipulate) {
        MemberManipulationViewModel vm = new MemberManipulationViewModel();
        vm.setPageTitle(pageTitle);

        vm.setItemToManipulate(itemToManipulate);
        vm.initServices(services);
        vm.initSaveAction(saveAction);

        return vm;
    }

    @ModelOperation
    public void initSaveAction(MemberManipulationViewModel model, ISaveAction<Member> saveAction) {
        this.saveAction = saveAction;
    }

    @ModelOperation
    public void initServices(MemberManipulationViewModel model, IPlatformServices services) {
        this.services = services;
    }

    @ComputedProperty
    public static boolean valid(Member itemToManipulate) {
        String userName = itemToManipulate.getUserName();
        return isNonEmptyField(userName) && !userName.contains(":")
                && isNonEmptyField(itemToManipulate.getFirstName())
                && isNonEmptyField(itemToManipulate.getLastName())
                && isNonEmptyField(itemToManipulate.getMailAddress())
                && isNonEmptyField(itemToManipulate.getPassword());
    }

    @Function
    public void save(MemberManipulationViewModel model) {
        Member member = model.getItemToManipulate();

        if (!model.isValid()) {
            services.toastWarning("Please fill all form fields");
            return;
        }

        saveAction.save(member);
    }
    
    private static boolean isNonEmptyField(String field)
    {
        if(field == null) {
            return false;
        }
        
        return !field.trim().equals("");
    }
}
