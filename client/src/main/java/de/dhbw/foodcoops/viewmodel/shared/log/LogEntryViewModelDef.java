package de.dhbw.foodcoops.viewmodel.shared.log;

import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "LogEntryViewModel", targetId = "", instance = true,
        properties = {
            @Property(name = "date", type = String.class),
            @Property(name = "time", type = String.class),
            @Property(name = "typeName", type = String.class),
            @Property(name = "typeClass", type = String.class),
            @Property(name = "message", type = String.class)
        })
public class LogEntryViewModelDef {

}
