package de.dhbw.foodcoops.viewmodel.shared;

import de.dhbw.foodcoops.client.resulthandler.decorator.ResultHandlerToastLogDecorator;
import de.dhbw.foodcoops.database.IRepository;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;
import de.dhbw.foodcoops.js.OnsenUITemplates;
import de.dhbw.foodcoops.log.ILogger;
import de.dhbw.foodcoops.model.ProductOffer;
import de.dhbw.foodcoops.model.WarehouseItem;
import de.dhbw.foodcoops.viewmodel.shared.ISaveAction;
import de.dhbw.foodcoops.viewmodel.shared.WarehouseItemManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.shared.WarehouseItemManipulationViewModel;
import de.dhbw.foodcoops.viewmodel.shared.WarehouseItemRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.WarehouseItemRootViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ItemAction;
import de.dhbw.foodcoops.viewmodel.shared.list.ListItemViewModel;
import de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel;
import java.util.List;
import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "WarehouseItemRootViewModel", instance = true, targetId = "",
        properties = {
            @Property(name = "loading", type = boolean.class),
            @Property(name = "adminApp", type = boolean.class),

            @Property(name = "warehouseItemManipulation", type = WarehouseItemManipulationViewModel.class),
            @Property(name = "warehouseItemDetails", type = WarehouseItem.class),
            @Property(name = "warehouseItemList", type = de.dhbw.foodcoops.viewmodel.shared.list.ListViewModel.class)
        })
public class WarehouseItemRootViewModelDef {

    private IPlatformServices services;
    private ILogger logger;
    private IRepository<WarehouseItem> warehouseItemRepository;
    private IRepository<ProductOffer> productOfferRepository;

    public static WarehouseItemRootViewModel create(IPlatformServices services, ILogger logger,
            IRepository<WarehouseItem> warehouseItemRepository, IRepository<ProductOffer> productOfferRepository) {
        WarehouseItemRootViewModel vm = new WarehouseItemRootViewModel();
        vm.setLoading(true);

        boolean isAdminApp = services.getPreferences("app.id", "").equals("AdminApp");
        vm.setAdminApp(isAdminApp);

        vm.initServices(services);
        vm.initLogger(logger);
        vm.initWarehouseItemRepository(warehouseItemRepository);
        vm.initProductOfferRepository(productOfferRepository);

        ListViewModel warehouseItemList = new ListViewModel();
        vm.setWarehouseItemList(warehouseItemList);
        vm.setWarehouseItemDetails(new WarehouseItem());

        return vm;
    }

    @ModelOperation
    public void initWarehouseItemRepository(WarehouseItemRootViewModel model, IRepository<WarehouseItem> repository) {
        this.warehouseItemRepository = repository;
    }

    @ModelOperation
    public void initProductOfferRepository(WarehouseItemRootViewModel model, IRepository<ProductOffer> repository) {
        this.productOfferRepository = repository;
    }

    @ModelOperation
    public void initServices(WarehouseItemRootViewModel model, IPlatformServices services) {
        this.services = services;

        model.getWarehouseItemList().initServices(services);
    }

    @ModelOperation
    public void initLogger(WarehouseItemRootViewModel model, ILogger logger) {
        this.logger = logger;
    }

    @Function
    public void navigateToAddPage(WarehouseItemRootViewModel model) {
        ISaveAction<WarehouseItem> saveAction = (newWarehouseItem) -> {
            IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> saveResultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Lagerelement erfolgreich gespeichert", "Fehler beim Speichern des Lagerelements",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            warehouseItemRepository.add(newWarehouseItem, saveResultHandler);
        };

        WarehouseItemManipulationViewModel manipulationVM = WarehouseItemManipulationViewModelDef.createAddPage(services, saveAction, productOfferRepository);
        model.setWarehouseItemManipulation(manipulationVM);

        OnsenUITemplates.switchPage("shared/warehouseItem/manipulate.html");
    }

    private static void refreshAndGoToHome(WarehouseItemRootViewModel model, IPlatformServices services) {
        model.refreshWarehouseItemList();
        OnsenUITemplates.goBackOnePage();
    }

    @ModelOperation
    public void navigateToEditPage(WarehouseItemRootViewModel model, WarehouseItem warehouseItemToEdit) {
        ISaveAction<WarehouseItem> saveAction = (editedWarehouseItem) -> {
            IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                    "Lagerelement erfolgreich gespeichert", "Fehler beim Speichern des Lagerelements",
                    (result) -> refreshAndGoToHome(model, services),
                    (exception) -> refreshAndGoToHome(model, services));

            warehouseItemRepository.update(editedWarehouseItem, resultHandler);
            refreshAndGoToHome(model, services);
        };

        WarehouseItemManipulationViewModel manipulationVM = WarehouseItemManipulationViewModelDef.createEditPage(warehouseItemToEdit.clone(), services, saveAction, productOfferRepository);
        model.setWarehouseItemManipulation(manipulationVM);

        OnsenUITemplates.switchPage("shared/warehouseItem/manipulate.html");
    }

    @Function
    @ModelOperation
    public void refreshWarehouseItemList(WarehouseItemRootViewModel model) {
        model.setLoading(true);

        IResultHandler<List<WarehouseItem>> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                "Lagerelement-Liste erfolgreich geladen", "Fehler beim Laden der Lagerelemente",
                (result) -> {
                    model.getWarehouseItemList().getItems().clear();
                    for (WarehouseItem warehouseItem : result) {
                        if (warehouseItem.getAmount() > 0) {
                            ListItemViewModel listItem = createListItemFor(model, warehouseItemRepository, services, logger, warehouseItem);
                            model.getWarehouseItemList().getItems().add(listItem);
                        }
                    }

                    model.setLoading(false);
                },
                (exception) -> {
                    model.setLoading(false);
                });

        warehouseItemRepository.getAll(resultHandler);
    }

    @ModelOperation
    public void navigateToDetailsPage(WarehouseItemRootViewModel model, WarehouseItem warehouseItem) {
        model.setWarehouseItemDetails(warehouseItem);
        OnsenUITemplates.switchPage("shared/warehouseItem/details.html");
    }

    private static ListItemViewModel createListItemFor(WarehouseItemRootViewModel model, IRepository<WarehouseItem> repository, IPlatformServices services, ILogger logger, WarehouseItem warehouseItem) {
        String itemTitle = warehouseItem.getId() + ": " + warehouseItem.getOffer().getProduct().getName();
        String itemDescription = "Menge: " + warehouseItem.getAmount();

        ListItemViewModel listItem = new ListItemViewModel(itemTitle, itemDescription);
        listItem.initDetailsAction(() -> model.navigateToDetailsPage(warehouseItem));

        if (model.isAdminApp()) {
            ItemAction editAction = new ItemAction("md-edit");
            editAction.init(() -> model.navigateToEditPage(warehouseItem));
            listItem.getActions().add(editAction);

            ItemAction deleteAction = new ItemAction("md-delete");
            deleteAction.init(() -> {
                services.confirmByUser("Lagerelement wirklich löschen?", () -> {
                    IResultHandler<de.dhbw.foodcoops.database.resulthandling.Void> resultHandler = new ResultHandlerToastLogDecorator<>(services, logger,
                            "Lagerelement erfolgreich gelöscht", "Fehler beim Löschen des Lagerelements",
                            (result) -> model.refreshWarehouseItemList(),
                            (exception) -> model.refreshWarehouseItemList());

                    repository.remove(warehouseItem, resultHandler);
                });
            });
            listItem.getActions().add(deleteAction);
        }

        return listItem;
    }
}
