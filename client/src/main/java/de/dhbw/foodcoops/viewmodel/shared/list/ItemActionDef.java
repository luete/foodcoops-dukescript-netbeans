
package de.dhbw.foodcoops.viewmodel.shared.list;

import net.java.html.json.Function;
import net.java.html.json.Model;
import net.java.html.json.ModelOperation;
import net.java.html.json.Property;

@Model(className = "ItemAction", instance = true, targetId = "",
        properties = {
            @Property(name="iconName", type=String.class)
        })
public class ItemActionDef {

    private Runnable action;
    
    @ModelOperation
    public void init(ItemAction model, Runnable action)
    {
        this.action = action;
    }
    
    @Function
    public void perform()
    {
        action.run();
    }
}
