package de.dhbw.foodcoops.client.resulthandler.decorator;

import de.dhbw.foodcoops.database.resulthandling.DelegateResultHandler;
import de.dhbw.foodcoops.database.resulthandling.IParameterizedAction;
import de.dhbw.foodcoops.database.resulthandling.IResultHandler;
import de.dhbw.foodcoops.js.IPlatformServices;

public class ResultHandlerToastDecorator<T> implements  IResultHandler<T>{

    private final IPlatformServices services;

    private final String successMessage;
    private final String errorMessage;
    private final IResultHandler<T> handlerToDecorate;

    public ResultHandlerToastDecorator(IPlatformServices services, String successMessage, String errorMessage,
                IParameterizedAction<T> successAction, IParameterizedAction<Exception> failureAction) {
        this(services, successMessage, errorMessage, new DelegateResultHandler<>(successAction, failureAction));
    }
    
    public ResultHandlerToastDecorator(IPlatformServices services, String successMessage, String errorMessage, IResultHandler<T> handlerToDecorate) {
        this.services = services;
        this.successMessage = successMessage;
        this.errorMessage = errorMessage;
        this.handlerToDecorate = handlerToDecorate;
    }

    @Override
    public void onSuccess(T result) {
        services.toastInfo(successMessage);
        handlerToDecorate.onSuccess(result);
    }

    @Override
    public void onError(Exception ex) {
        services.toastWarning(errorMessage + ": " + ex.getMessage());
        handlerToDecorate.onError(ex);
    }
}
