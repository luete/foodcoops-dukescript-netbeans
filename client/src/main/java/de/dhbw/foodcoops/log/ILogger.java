package de.dhbw.foodcoops.log;

public interface ILogger {
    void log(String type, String message);
    void success(String message);
    void failure(String message);
}
