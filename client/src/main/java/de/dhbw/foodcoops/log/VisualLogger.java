package de.dhbw.foodcoops.log;

import de.dhbw.foodcoops.js.DateService;
import de.dhbw.foodcoops.viewmodel.shared.log.LogEntryViewModel;
import de.dhbw.foodcoops.viewmodel.shared.log.LogViewModel;

public class VisualLogger implements ILogger {

    private final LogViewModel viewModel;

    public VisualLogger(LogViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public void log(String type, String message) {
        DateService dateService = new DateService();
        
        String date = dateService.currentDateString();
        String time = dateService.currentTimeString();
        viewModel.getLogEntries().add(new LogEntryViewModel(date, time, type, "log-" + type.toLowerCase(), message));
    }

    @Override
    public void success(String message) {
        this.log("Success", message);
    }

    @Override
    public void failure(String message) {
        this.log("Failure", message);
    }
}
