package de.dhbw.foodcoops.model;

import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "ShoppingCartItem", instance = true, targetId = "",
        properties = {
            // Produktbild evtl. hinzufügen
            @Property(name = "id", type = int.class),
            @Property(name = "offer", type = ProductOffer.class),
            @Property(name = "amount", type = int.class),
            @Property(name = "user", type = Member.class)
        })
public class ShoppingCartItemDef {
    @ComputedProperty
    public static double totalPrice(int amount, ProductOffer offer) {
        return amount * offer.getPricePerUnit();
    }
    
    @ComputedProperty
    public static boolean valid(int amount, ProductOffer offer, Member user) {
        return offer != null && user != null && amount > 0;
    }
    
    public static ShoppingCartItem create(int amount, ProductOffer offer, Member user) {
        ShoppingCartItem item = new ShoppingCartItem();
        
        item.setAmount(amount);
        item.setOffer(offer);
        item.setUser(user);
        
        return item;
    }
}
