package de.dhbw.foodcoops.model;

import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "ExtractionItem", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "warehouseItem", type = WarehouseItem.class),
            @Property(name = "amount", type = int.class)
        })
public class ExtractionItemDef {
    @ComputedProperty
    public static boolean valid(int amount, WarehouseItem warehouseItem) {
        return amount > 0 && warehouseItem != null;
    }
    
    public static ExtractionItem create(int amount, WarehouseItem warehouseItem) {
        ExtractionItem item = new ExtractionItem();
        
        item.setAmount(amount);
        item.setWarehouseItem(warehouseItem);
        
        return item;
    }
}
