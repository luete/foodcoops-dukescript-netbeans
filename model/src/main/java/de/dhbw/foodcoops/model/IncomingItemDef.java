package de.dhbw.foodcoops.model;

import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "IncomingItem", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "offer", type = ProductOffer.class),
            @Property(name = "amount", type = int.class)
        })
public class IncomingItemDef {
    public static IncomingItem create(int amount, ProductOffer offer) {
        return new IncomingItem(0, offer, amount);
    }
    
    
    @ComputedProperty
    public static boolean valid(ProductOffer offer, int amount) {
        return offer != null && amount > 0;
    }
}
