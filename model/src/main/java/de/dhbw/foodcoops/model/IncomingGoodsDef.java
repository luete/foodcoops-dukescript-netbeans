package de.dhbw.foodcoops.model;

import java.util.List;
import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "IncomingGoods", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "contactPerson", type = Member.class),
            @Property(name = "incomingItems", type = IncomingItem.class, array = true),
            @Property(name = "creationDate", type = String.class)
        })
public class IncomingGoodsDef {
    @ComputedProperty
    public static boolean valid(Member contactPerson, List<IncomingItem> incomingItems, String creationDate) {
        return contactPerson != null && !incomingItems.isEmpty() && isNonEmptyField(creationDate);
    }
    
    private static boolean isNonEmptyField(String field)
    {
        if(field == null) {
            return false;
        }
        
        return !field.trim().equals("");
    }
}
