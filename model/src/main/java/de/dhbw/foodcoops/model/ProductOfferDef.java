package de.dhbw.foodcoops.model;

import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "ProductOffer", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "outdated", type = boolean.class),
            
            @Property(name = "supplierId", type = int.class),
            @Property(name = "product", type = Product.class),
            @Property(name = "pricePerUnit", type = double.class),
            @Property(name = "minimumOrderQuantity", type = int.class)
        })
public class ProductOfferDef {
    public static ProductOffer create(Product product, double pricePerUnit, int minimumOrderQuantity){
        return create(-1, product, pricePerUnit, minimumOrderQuantity);
    }
    
    public static ProductOffer create(int supplierId, Product product, double pricePerUnit, int minimumOrderQuantity){
        ProductOffer offer = new ProductOffer();
        offer.setProduct(product);
        offer.setPricePerUnit(pricePerUnit);
        offer.setMinimumOrderQuantity(minimumOrderQuantity);
        offer.setSupplierId(supplierId);
        
        return offer;
    }
    
    @ComputedProperty
    public static boolean valid(Product product, double pricePerUnit, int minimumOrderQuantity) {
        return product.isValid() && pricePerUnit >= 0 && minimumOrderQuantity >= 0;
    }
}
