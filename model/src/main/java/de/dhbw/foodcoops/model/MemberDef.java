package de.dhbw.foodcoops.model;

import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "Member", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "outdated", type = boolean.class),
            @Property(name = "admin", type = boolean.class),
            
            @Property(name = "userName", type = String.class),
            @Property(name = "password", type = String.class),
                
            @Property(name = "firstName", type = String.class),
            @Property(name = "lastName", type = String.class),
            @Property(name = "mailAddress", type = String.class)
        })
public class MemberDef {
    @ComputedProperty
    public static String displayName(String firstName, String lastName) {
        return firstName + " " + lastName;
    }
}
