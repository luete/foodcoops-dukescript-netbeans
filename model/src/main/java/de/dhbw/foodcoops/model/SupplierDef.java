package de.dhbw.foodcoops.model;

import net.java.html.json.ComputedProperty;
import net.java.html.json.Model;
import net.java.html.json.Property;

@Model(className = "Supplier", instance = true, targetId = "",
        properties = {
            @Property(name = "id", type = int.class),
            @Property(name = "outdated", type = boolean.class),
            
            @Property(name = "name", type = String.class),
            @Property(name = "contactPerson", type = String.class),
            @Property(name = "street", type = String.class),
            @Property(name = "location", type = String.class),
            @Property(name = "postalCode", type = String.class),
            @Property(name = "phoneNumber", type = String.class),
            @Property(name = "mailAddress", type = String.class),
            
            @Property(name = "productOffers", type = de.dhbw.foodcoops.model.ProductOffer.class, array = true)
        })
public class SupplierDef {
    /**
     * asdf
     * @param name
     * @param contactPerson
     * @param street
     * @param location
     * @param postalCode
     * @param phoneNumber
     * @param mailAddress
     * @return 
     */
    @ComputedProperty
    public static boolean valid(String name, String contactPerson, String street, String location,
                                  String postalCode, String phoneNumber, String mailAddress) {
        return isNonEmptyField(name) && isNonEmptyField(contactPerson) && isNonEmptyField(street) && isNonEmptyField(location)
                && isNonEmptyField(postalCode) && isNonEmptyField(phoneNumber) && isNonEmptyField(mailAddress);
    }
    
    private static boolean isNonEmptyField(String field)
    {
        if(field == null) {
            return false;
        }
        
        return !field.trim().equals("");
    }
}
